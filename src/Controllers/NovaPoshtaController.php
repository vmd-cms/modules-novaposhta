<?php

namespace VmdCms\Modules\NovaPoshta\Controllers;

use Illuminate\Http\Request;
use VmdCms\Modules\NovaPoshta\Entity\NovaPoshtaSearch;

class NovaPoshtaController
{
    public function searchCity(Request $request){
        $request->validate([
            'needle' => 'required|string|min:3|max:32'
        ]);
        $service = new NovaPoshtaSearch();
        $result = $service->getAddresses($request->get('needle'));
        $options = [];
        if(is_countable($result->getItems())){
            foreach ($result->getItems() as $item){
                $options[] = [
                    'ref' => $item->getRef(),
                    'delivery_id' => $item->getDeliveryCity(),
                    'title' => $item->getPresent()
                ];
            }
        }
        $optionView = view('vmd_cms::admin.services.novaposhta.custom_options',['options' => $options])->render();
        return json_encode([
            'options' => $optionView
        ]);
    }

    public function searchWarehouse(Request $request){
        $request->validate([
            'cityDeliveryId' => 'required|string|min:15|max:64',
            'page' => 'required|int',
        ]);
        $page = $request->get('page',1);
        if($page < 1) $page = 1;
        $service = new NovaPoshtaSearch();
        $result = $service->getWarehouses($request->get('cityDeliveryId'),100,$page);
        $options = [];
        if(is_countable($result->getItems())){
            foreach ($result->getItems() as $item){
                $options[] = [
                    'ref' => $item->getRef(),
                    'title' => $item->getDescription()
                ];
            }
        }
        $hasNext = count($options) == 100;
        $optionView = view('vmd_cms::admin.services.novaposhta.custom_options',['options' => $options])->render();
        return json_encode([
            'options' => $optionView,
            'hasItems' => count($options) > 0,
            'hasNext' => $hasNext
        ]);
    }
}
