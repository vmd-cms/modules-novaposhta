<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\Modules\NovaPoshta\Models\City;

class AddressDTO
{
    /**
     * @var string
     */
    private $present;

    /**
     * @var int
     */
    private $warehouses;

    /**
     * @var string
     */
    private $mainDescription;

    /**
     * @var string
     */
    private $area;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $settlementTypeCode;

    /**
     * @var string
     */
    private $ref;

    /**
     * @var string
     */
    private $deliveryCity;

    /**
     * @var bool
     */
    private $addressDeliveryAllowed;

    /**
     * @var bool
     */
    private $streetsAvailability;

    /**
     * @var string
     */
    private $parentRegionTypes;

    /**
     * @var string
     */
    private $parentRegionCode;

    /**
     * @var string
     */
    private $regionTypes;

    /**
     * @var string
     */
    private $regionTypesCode;

    public function __construct($addressObj)
    {
        if($addressObj instanceof City){
            $this->mapFromCityObj($addressObj);
        }else{
            $this->mapFromResponseObj($addressObj);
        }

    }

    protected function mapFromResponseObj($addressObj){
        $this->present = $addressObj->Present ?? null;
        $this->warehouses = $addressObj->Warehouses ?? null;
        $this->mainDescription = $addressObj->MainDescription ?? null;
        $this->area = $addressObj->Area ?? null;
        $this->region = $addressObj->Region ?? null;
        $this->settlementTypeCode = $addressObj->SettlementTypeCode ?? null;
        $this->ref = $addressObj->Ref ?? null;
        $this->deliveryCity = $addressObj->DeliveryCity ?? null;
        $this->addressDeliveryAllowed = $addressObj->AddressDeliveryAllowed ?? null;
        $this->streetsAvailability = $addressObj->StreetsAvailability ?? null;
        $this->parentRegionTypes = $addressObj->ParentRegionTypes ?? null;
        $this->parentRegionCode = $addressObj->ParentRegionCode ?? null;
        $this->regionTypes = $addressObj->RegionTypes ?? null;
        $this->regionTypesCode = $addressObj->RegionTypesCode ?? null;
    }

    protected function mapFromCityObj($addressObj){
        $this->present = $addressObj->info->description ?? null;
        $this->mainDescription = $addressObj->info->description ?? null;
        $this->area = $addressObj->info->area_description ?? null;
        $this->ref = $addressObj->ref ?? null;
        $this->deliveryCity = $addressObj->ref ?? null;
    }

    /**
     * @return string|null
     */
    public function getPresent(): ?string
    {
        return $this->present;
    }

    /**
     * @return int
     */
    public function getWarehouses(): int
    {
        return $this->warehouses;
    }

    /**
     * @return string|null
     */
    public function getMainDescription(): ?string
    {
        return $this->mainDescription;
    }

    /**
     * @return string|null
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getSettlementTypeCode(): ?string
    {
        return $this->settlementTypeCode;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    /**
     * @return bool
     */
    public function isAddressDeliveryAllowed(): bool
    {
        return $this->addressDeliveryAllowed;
    }

    /**
     * @return bool
     */
    public function isStreetsAvailability(): bool
    {
        return $this->streetsAvailability;
    }

    /**
     * @return string|null
     */
    public function getParentRegionTypes(): ?string
    {
        return $this->parentRegionTypes;
    }

    /**
     * @return string|null
     */
    public function getParentRegionCode(): ?string
    {
        return $this->parentRegionCode;
    }

    /**
     * @return string|null
     */
    public function getRegionTypes(): ?string
    {
        return $this->regionTypes;
    }

    /**
     * @return string|null
     */
    public function getRegionTypesCode(): ?string
    {
        return $this->regionTypesCode;
    }

}
