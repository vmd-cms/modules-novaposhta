<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Admin;

class DataCityDTO
{
    /**
     * @var string|null
     */
    protected $title;

    /**
     * @var string|null
     */
    protected $deliveryId;

    /**
     * @var string|null
     */
    protected $ref;

    public function __construct(string $title = null, string $deliveryId = null, string $ref = null)
    {
        $this->title = $title;
        $this->deliveryId = $deliveryId;
        $this->ref = $ref;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getDeliveryId(): ?string
    {
        return $this->deliveryId;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

}
