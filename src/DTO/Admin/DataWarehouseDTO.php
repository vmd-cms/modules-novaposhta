<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Admin;

class DataWarehouseDTO
{
    /**
     * @var string|null
     */
    protected $title;


    /**
     * @var string|null
     */
    protected $ref;

    public function __construct(string $title = null, string $ref = null)
    {
        $this->title = $title;
        $this->ref = $ref;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }
}
