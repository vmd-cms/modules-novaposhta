<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Counterparty;

use VmdCms\Modules\NovaPoshta\Models\Settings\Counterparty;
use VmdCms\Modules\NovaPoshta\Models\Settings\CounterpartyContact;

class CounterpartyContactDTO
{
    /**
     * @var string|null
     */
    protected $ref;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $phones;

    /**
     * @var string|null
     */
    protected $email;

    /**
     * @var string|null
     */
    protected $firstName;

    /**
     * @var string|null
     */
    protected $lastName;

    /**
     * @var string|null
     */
    protected $middleName;

    /**
     * CounterpartyDTO constructor.
     * @param array|Counterparty $params
     */
    public function __construct($params)
    {
        if($params instanceof CounterpartyContact){
            $this->mapFromObj($params);
        }else{
            $this->mapFromResponseObj($params);
        }
    }

    protected function mapFromObj(CounterpartyContact $params)
    {
        $this->ref = $params->ref ?? null;
        $this->firstName = $params->first_name ?? null;
        $this->lastName = $params->last_name ?? null;
        $this->middleName = $params->middle_name ?? null;
        $this->phones = $params->phones ?? null;
        $this->email = $params->email ?? null;
    }

    protected function mapFromResponseObj($params)
    {
        $this->ref = $params->Ref ?? null;
        $this->firstName = $params->FirstName ?? null;
        $this->lastName = $params->LastName ?? null;
        $this->middleName = $params->MiddleName ?? null;
        $this->phones = $params->Phones ?? null;
        $this->email = $params->Email ?? null;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @return string|null
     */
    public function getPhones(): ?string
    {
        return $this->phones;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

}
