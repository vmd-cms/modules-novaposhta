<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Counterparty;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class CounterpartyContactDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param CounterpartyContactDTO $dto
     */
    public function append(CounterpartyContactDTO $dto)
    {
        $this->collection->add($dto);
    }

}
