<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Counterparty;

use VmdCms\Modules\NovaPoshta\Models\Settings\Counterparty;

class CounterpartyDTO
{
    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $ref;

    /**
     * @var string|null
     */
    protected $city;

    /**
     * @var string|null
     */
    protected $counterparty;

    /**
     * @var string|null
     */
    protected $firstName;

    /**
     * @var string|null
     */
    protected $lastName;

    /**
     * @var string|null
     */
    protected $middleName;

    /**
     * @var string|null
     */
    protected $ownershipFormRef;

    /**
     * @var string|null
     */
    protected $ownershipFormDescription;

    /**
     * @var string|null
     */
    protected $edrpou;

    /**
     * @var string|null
     */
    protected $counterpartyType;

    /**
     * CounterpartyDTO constructor.
     * @param array|Counterparty $params
     */
    public function __construct($params)
    {
        if($params instanceof Counterparty){
            $this->mapFromObj($params);
        }else{
            $this->mapFromResponseObj($params);
        }
    }

    protected function mapFromObj(Counterparty $params)
    {
        $this->ref = $params->ref ?? null;
        $this->firstName = $params->first_name ?? null;
        $this->lastName = $params->last_name ?? null;
        $this->middleName = $params->middle_name ?? null;
        $this->counterpartyType = $params->counterparty_type ?? null;
    }

    protected function mapFromResponseObj($params)
    {
        $this->description = $params->Description ?? null;
        $this->ref = $params->Ref ?? null;
        $this->city = $params->City ?? null;
        $this->counterparty = $params->Counterparty ?? null;
        $this->firstName = $params->FirstName ?? null;
        $this->lastName = $params->LastName ?? null;
        $this->middleName = $params->MiddleName ?? null;
        $this->ownershipFormRef = $params->OwnershipFormRef ?? null;
        $this->ownershipFormDescription = $params->OwnershipFormDescription ?? null;
        $this->edrpou = $params->EDRPOU ?? null;
        $this->counterpartyType = $params->CounterpartyType ?? null;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getCounterparty(): ?string
    {
        return $this->counterparty;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @return string|null
     */
    public function getOwnershipFormRef(): ?string
    {
        return $this->ownershipFormRef;
    }

    /**
     * @return string|null
     */
    public function getOwnershipFormDescription(): ?string
    {
        return $this->ownershipFormDescription;
    }

    /**
     * @return string|null
     */
    public function getEdrpou(): ?string
    {
        return $this->edrpou;
    }

    /**
     * @return string|null
     */
    public function getCounterpartyType(): ?string
    {
        return $this->counterpartyType;
    }

}
