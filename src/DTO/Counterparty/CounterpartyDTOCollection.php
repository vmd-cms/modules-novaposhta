<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Counterparty;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class CounterpartyDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param CounterpartyDTO $dto
     */
    public function append(CounterpartyDTO $dto)
    {
        $this->collection->add($dto);
    }

}
