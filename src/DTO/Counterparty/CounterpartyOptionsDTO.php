<?php

namespace VmdCms\Modules\NovaPoshta\DTO\Counterparty;

class CounterpartyOptionsDTO
{
    /**
     * @var bool
     */
    protected $fillingWarranty;

    /**
     * @var bool
     */
    protected $addressDocumentDelivery;

    /**
     * @var bool
     */
    protected $canPayTheThirdPerson;

    /**
     * @var bool
     */
    protected $canAfterpaymentOnGoodsCost;

    /**
     * @var bool
     */
    protected $canNonCashPayment;

    /**
     * @var bool
     */
    protected $canCreditDocuments;

    /**
     * @var bool
     */
    protected $canEWTransporter;

    /**
     * @var bool
     */
    protected $canSignedDocuments;

    /**
     * @var bool
     */
    protected $hideDeliveryCost;

    /**
     * @var bool
     */
    protected $blockInternationalSenderLKK;

    /**
     * @var bool
     */
    protected $internationalDelivery;

    /**
     * @var bool
     */
    protected $pickupService;

    /**
     * @var bool
     */
    protected $canSameDayDelivery;

    /**
     * @var bool
     */
    protected $canSameDayDeliveryStandart;

    /**
     * @var bool
     */
    protected $canForwardingService;

    /**
     * @var bool
     */
    protected $showDeliveryByHand;

    /**
     * @var bool
     */
    protected $deliveryByHand;

    /**
     * @var bool
     */
    protected $partialReturn;

    /**
     * @var bool
     */
    protected $loyaltyProgram;

    /**
     * @var bool
     */
    protected $canSentFromPostomat;

    /**
     * @var bool
     */
    protected $descentFromFloor;

    /**
     * @var bool
     */
    protected $backDeliveryValuablePapers;

    /**
     * @var bool
     */
    protected $backwardDeliverySubtypesDocuments;

    /**
     * @var bool
     */
    protected $afterpaymentType;

    /**
     * @var bool
     */
    protected $creditDocuments;

    /**
     * @var bool
     */
    protected $signedDocuments;

    /**
     * @var bool
     */
    protected $services;

    /**
     * @var bool
     */
    protected $internationalDeliveryServiceType;

    /**
     * @var bool
     */
    protected $printMarkingAllowedTypes;

    /**
     * @var bool
     */
    protected $inventoryOrder;

    /**
     * @var bool
     */
    protected $debtor;

    /**
     * @var bool
     */
    protected $debtorParams;

    /**
     * @var bool
     */
    protected $calculationByFactualWeight;

    /**
     * @var bool
     */
    protected $transferPricingConditions;

    /**
     * @var bool
     */
    protected $businessClient;

    /**
     * @var bool
     */
    protected $haveMoneyWallets;

    /**
     * @var bool
     */
    protected $customerReturn;

    /**
     * @var bool
     */
    protected $dayCustomerReturn;

    /**
     * @var bool
     */
    protected $mainCounterparty;

    /**
     * @var bool
     */
    protected $securePayment;

    public function __construct($params)
    {
        if(!is_array($params)){
            $this->mapFromResponseObj($params);
        }
    }

    protected function mapFromResponseObj($params)
    {
        $this->fillingWarranty = $params->FillingWarranty ?? false;
        $this->addressDocumentDelivery = $params->AddressDocumentDelivery ?? false;
        $this->canPayTheThirdPerson = $params->CanPayTheThirdPerson ?? false;
        $this->canAfterpaymentOnGoodsCost = $params->CanAfterpaymentOnGoodsCost ?? false;
        $this->canNonCashPayment = $params->CanNonCashPayment ?? false;
        $this->canCreditDocuments = $params->CanCreditDocuments ?? false;
        $this->canEWTransporter = $params->CanEWTransporter ?? false;
        $this->canSignedDocuments = $params->CanSignedDocuments ?? false;
        $this->hideDeliveryCost = $params->HideDeliveryCost ?? false;
        $this->blockInternationalSenderLKK = $params->BlockInternationalSenderLKK ?? false;
        $this->internationalDelivery = $params->InternationalDelivery ?? false;
        $this->pickupService = $params->PickupService ?? false;
        $this->canSameDayDelivery = $params->CanSameDayDelivery ?? false;
        $this->canSameDayDeliveryStandart = $params->CanSameDayDeliveryStandart ?? false;
        $this->canForwardingService = $params->CanForwardingService ?? false;
        $this->showDeliveryByHand = $params->ShowDeliveryByHand ?? false;
        $this->deliveryByHand = $params->DeliveryByHand ?? false;
        $this->partialReturn = $params->PartialReturn ?? false;
        $this->loyaltyProgram = $params->LoyaltyProgram ?? false;
        $this->canSentFromPostomat = $params->CanSentFromPostomat ?? false;
        $this->descentFromFloor = $params->DescentFromFloor ?? false;
        $this->backDeliveryValuablePapers = $params->BackDeliveryValuablePapers ?? false;
        $this->backwardDeliverySubtypesDocuments = $params->BackwardDeliverySubtypesDocuments ?? false;
        $this->afterpaymentType = $params->AfterpaymentType ?? false;
        $this->creditDocuments = $params->CreditDocuments ?? false;
        $this->signedDocuments = $params->SignedDocuments ?? false;
        $this->services = $params->Services ?? false;
        $this->internationalDeliveryServiceType = $params->InternationalDeliveryServiceType ?? false;
        $this->printMarkingAllowedTypes = $params->PrintMarkingAllowedTypes ?? false;
        $this->inventoryOrder = $params->InventoryOrder ?? false;
        $this->debtor = $params->Debtor ?? false;
        $this->debtorParams = $params->DebtorParams ?? false;
        $this->calculationByFactualWeight = $params->CalculationByFactualWeight ?? false;
        $this->transferPricingConditions = $params->TransferPricingConditions ?? false;
        $this->businessClient = $params->BusinessClient ?? false;
        $this->haveMoneyWallets = $params->HaveMoneyWallets ?? false;
        $this->customerReturn = $params->CustomerReturn ?? false;
        $this->dayCustomerReturn = $params->DayCustomerReturn ?? false;
        $this->mainCounterparty = $params->MainCounterparty ?? false;
        $this->securePayment = $params->SecurePayment ?? false;
    }
}
/*
    FillingWarranty
    AddressDocumentDelivery
    CanPayTheThirdPerson
    CanAfterpaymentOnGoodsCost
    CanNonCashPayment
    CanCreditDocuments
    CanEWTransporter
    CanSignedDocuments
    HideDeliveryCost
    BlockInternationalSenderLKK
    InternationalDelivery
    PickupService
    CanSameDayDelivery
    CanSameDayDeliveryStandart
    CanForwardingService
    ShowDeliveryByHand
    DeliveryByHand
    PartialReturn
    LoyaltyProgram
    CanSentFromPostomat
    DescentFromFloor
    BackDeliveryValuablePapers
    BackwardDeliverySubtypesDocuments
    AfterpaymentType
    CreditDocuments
    SignedDocuments
    Services
    InternationalDeliveryServiceType
    PrintMarkingAllowedTypes
    InventoryOrder
    Debtor
    DebtorParams
    CalculationByFactualWeight
    TransferPricingConditions
    BusinessClient
    HaveMoneyWallets
    CustomerReturn
    DayCustomerReturn
    MainCounterparty
    SecurePayment

 */
