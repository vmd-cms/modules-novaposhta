<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\Modules\NovaPoshta\Models\NovaPoshtaSettings;
use VmdCms\Modules\NovaPoshta\Models\Settings\ErrorCode;

class ErrorCodeDTO
{
    /**
     * @var string|null
     */
    protected $code;

    /**
     * @var string|null
     */
    protected $error;

    /**
     * @var string|null
     */
    protected $descriptionRu;

    /**
     * @var string|null
     */
    protected $descriptionUa;

    public function __construct($params)
    {
        if($params instanceof ErrorCode){
            $this->mapFromObj($params);
        }else{
            $this->mapFromResponseObj($params);
        }
    }
    protected function mapFromObj(ErrorCode $params)
    {
        $this->code = $params->title ?? null;
        $this->error = $params->error ?? null;
        $this->descriptionUa = $params->description_ua ?? null;
        $this->descriptionRu = $params->description_ru ?? null;
    }

    protected function mapFromResponseObj($params)
    {
        $this->code = $params->MessageCode ?? null;
        $this->error = $params->MessageText ?? null;
        $this->descriptionUa = $params->MessageDescriptionUA ?? null;
        $this->descriptionRu = $params->MessageDescriptionRU ?? null;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return string|null
     */
    public function getDescriptionRu(): ?string
    {
        return $this->descriptionRu;
    }

    /**
     * @return string|null
     */
    public function getDescriptionUa(): ?string
    {
        return $this->descriptionUa;
    }


}
