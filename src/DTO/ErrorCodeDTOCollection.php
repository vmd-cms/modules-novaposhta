<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class ErrorCodeDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param ErrorCodeDTO $dto
     */
    public function append(ErrorCodeDTO $dto)
    {
        $this->collection->add($dto);
    }

}
