<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use Carbon\Carbon;
use VmdCms\Modules\NovaPoshta\Models\InternetDocument;

class InternetDocumentDTO
{
    /**
     * @var string|null
     */
    protected $ref;

    /**
     * @var string|null
     */
    protected $payerType;

    /**
     * @var string|null
     */
    protected $paymentMethod;

    /**
     * @var string|null
     */
    protected $dateTime;

    /**
     * @var string|null
     */
    protected $cargoType;

    /**
     * @var string|null
     */
    protected $volumeGeneral;

    /**
     * @var string|null
     */
    protected $weight;

    /**
     * @var string|null
     */
    protected $serviceType;

    /**
     * @var string|null
     */
    protected $seatsAmount;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $cost;

    /**
     * @var string|null
     */
    protected $citySender;

    /**
     * @var string|null
     */
    protected $sender;

    /**
     * @var string|null
     */
    protected $senderAddress;

    /**
     * @var string|null
     */
    protected $contactSender;

    /**
     * @var string|null
     */
    protected $sendersPhone;

    /**
     * @var string|null
     */
    protected $recipientCityName;

    /**
     * @var string|null
     */
    protected $recipientArea;

    /**
     * @var string|null
     */
    protected $recipientAreaRegions;

    /**
     * @var string|null
     */
    protected $recipientAddressName;

    /**
     * @var string|null
     */
    protected $recipientHouse;

    /**
     * @var string|null
     */
    protected $recipientFlat;

    /**
     * @var string|null
     */
    protected $recipientName;

    /**
     * @var string|null
     */
    protected $recipientType;

    /**
     * @var string|null
     */
    protected $recipientsPhone;

    /**
     * @var string|null
     */
    protected $settlementType;

    /**
     * InternetDocumentDTO constructor.
     * @param array|InternetDocument $params
     */
    public function __construct($params)
    {
        if($params instanceof InternetDocument){
            $this->mapFromObj($params);
        }else{
            $this->mapFromResponseObj($params);
        }
    }

    protected function mapFromObj(InternetDocument $params)
    {
        $this->ref = $params->ref ?? null;
        $this->payerType = $params->payer_type ?? null;
        $this->paymentMethod = $params->payment_method ?? null;
        $this->dateTime = $params->date_time ?? null;
        $this->cargoType = $params->cargo_type ?? null;
        $this->volumeGeneral = $params->volume_general ?? null;
        $this->weight = $params->weight ?? null;
        $this->serviceType = $params->service_type ?? null;
        $this->seatsAmount = $params->seats_amount ?? null;
        $this->description = $params->description ?? null;
        $this->cost = $params->cost ?? null;
        $this->citySender = $params->city_sender ?? null;
        $this->sender = $params->sender ?? null;
        $this->senderAddress = $params->sender_address ?? null;
        $this->contactSender = $params->contact_sender ?? null;
        $this->sendersPhone = $params->senders_phone ?? null;
        $this->recipientCityName = $params->recipient_city_name ?? null;
        $this->recipientArea = $params->recipient_area ?? null;
        $this->recipientAreaRegions = $params->recipient_area_regions ?? null;
        $this->recipientAddressName = $params->recipient_address_name ?? null;
        $this->recipientHouse = $params->recipient_house ?? null;
        $this->recipientFlat = $params->recipient_flat ?? null;
        $this->recipientName = $params->recipient_name ?? null;
        $this->recipientType = $params->recipient_type ?? null;
        $this->recipientsPhone = $params->recipients_phone ?? null;
        $this->settlementType = $params->settlement_type ?? null;
    }

    protected function mapFromResponseObj($params)
    {
        $this->ref = $params->Ref ?? null;
        $this->payerType = $params->PayerType ?? null;
        $this->paymentMethod = $params->PaymentMethod ?? null;
        $this->dateTime = $params->DateTime ?? null;
        $this->cargoType = $params->CargoType ?? null;
        $this->volumeGeneral = $params->VolumeGeneral ?? null;
        $this->weight = $params->Weight ?? null;
        $this->serviceType = $params->ServiceType ?? null;
        $this->seatsAmount = $params->SeatsAmount ?? null;
        $this->description = $params->Description ?? null;
        $this->cost = $params->Cost ?? null;
        $this->citySender = $params->CitySender ?? null;
        $this->sender = $params->Sender ?? null;
        $this->senderAddress = $params->SenderAddress ?? null;
        $this->contactSender = $params->ContactSender ?? null;
        $this->sendersPhone = $params->SendersPhone ?? null;
        $this->recipientCityName = $params->RecipientCityName ?? null;
        $this->recipientArea = $params->RecipientArea ?? null;
        $this->recipientAreaRegions = $params->RecipientAreaRegions ?? null;
        $this->recipientAddressName = $params->recipientAddressName ?? null;
        $this->recipientHouse = $params->RecipientHouse ?? null;
        $this->recipientFlat = $params->RecipientFlat ?? null;
        $this->recipientName = $params->RecipientName ?? null;
        $this->recipientType = $params->RecipientType ?? null;
        $this->recipientsPhone = $params->RecipientsPhone ?? null;
        $this->settlementType = $params->SettlementType ?? null;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getPayerType(): ?string
    {
        return $this->payerType;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * @return string|null
     */
    public function getDateTime(): ?string
    {
        return $this->dateTime;
    }

    /**
     * @return string|null
     */
    public function getCargoType(): ?string
    {
        return $this->cargoType;
    }

    /**
     * @return string|null
     */
    public function getVolumeGeneral(): ?string
    {
        return $this->volumeGeneral;
    }

    /**
     * @return string|null
     */
    public function getWeight(): ?string
    {
        return $this->weight;
    }

    /**
     * @return string|null
     */
    public function getServiceType(): ?string
    {
        return $this->serviceType;
    }

    /**
     * @return string|null
     */
    public function getSeatsAmount(): ?string
    {
        return $this->seatsAmount;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getCost(): ?string
    {
        return $this->cost;
    }

    /**
     * @return string|null
     */
    public function getCitySender(): ?string
    {
        return $this->citySender;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @return string|null
     */
    public function getSenderAddress(): ?string
    {
        return $this->senderAddress;
    }

    /**
     * @return string|null
     */
    public function getContactSender(): ?string
    {
        return $this->contactSender;
    }

    /**
     * @return string|null
     */
    public function getSendersPhone(): ?string
    {
        return $this->sendersPhone;
    }

    /**
     * @return string|null
     */
    public function getRecipientCityName(): ?string
    {
        return $this->recipientCityName;
    }

    /**
     * @return string|null
     */
    public function getRecipientArea(): ?string
    {
        return $this->recipientArea;
    }

    /**
     * @return string|null
     */
    public function getRecipientAreaRegions(): ?string
    {
        return $this->recipientAreaRegions;
    }

    /**
     * @return string|null
     */
    public function getRecipientAddressName(): ?string
    {
        return $this->recipientAddressName;
    }

    /**
     * @return string|null
     */
    public function getRecipientHouse(): ?string
    {
        return $this->recipientHouse;
    }

    /**
     * @return string|null
     */
    public function getRecipientFlat(): ?string
    {
        return $this->recipientFlat;
    }

    /**
     * @return string|null
     */
    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    /**
     * @return string|null
     */
    public function getRecipientType(): ?string
    {
        return $this->recipientType;
    }

    /**
     * @return string|null
     */
    public function getRecipientsPhone(): ?string
    {
        return $this->recipientsPhone;
    }

    /**
     * @return string|null
     */
    public function getSettlementType(): ?string
    {
        return $this->settlementType;
    }

    public function toArray(){
        return [
            'Ref' => $this->ref,
            'NewAddress' => 1,
            'PayerType' => $this->payerType,
            'PaymentMethod' => $this->paymentMethod,
            'CargoType' => $this->cargoType,
            'VolumeGeneral' => $this->volumeGeneral,
            'Weight' => $this->weight,
            'ServiceType' => $this->serviceType,
            'SeatsAmount' => $this->seatsAmount,
            'Description' => $this->description,
            'Cost' => $this->cost,
            'CitySender' => $this->citySender,
            'Sender' => $this->sender,
            'SenderAddress' => $this->senderAddress,
            'ContactSender' => $this->contactSender,
            'SendersPhone' => $this->sendersPhone,
            'RecipientCityName' => $this->recipientCityName,
            'RecipientArea' => $this->recipientArea,
            'RecipientAreaRegions' => $this->recipientAreaRegions,
            'RecipientAddressName' => $this->recipientAddressName,
            'RecipientHouse' => $this->recipientHouse,
            'RecipientFlat' => $this->recipientFlat,
            'RecipientName' => $this->recipientName,
            'RecipientType' => $this->recipientType,
            'RecipientsPhone' => $this->recipientsPhone,
            'DateTime' => $this->dateTime ? Carbon::parse($this->dateTime)->format('d.m.Y') : null,
        ];
    }

}
