<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\Modules\NovaPoshta\Models\NovaPoshtaSettings;

class TypeItemDTO
{
    /**
     * @var string|null
     */
    protected $ref;

    /**
     * @var string|null
     */
    protected $description;

    public function __construct($params)
    {
        if($params instanceof NovaPoshtaSettings){
            $this->mapFromObj($params);
        }else{
            $this->mapFromResponseObj($params);
        }
    }
    protected function mapFromObj(NovaPoshtaSettings $params)
    {
        $this->ref = $params->ref ?? null;
        $this->description = $params->description ?? null;
    }

    protected function mapFromResponseObj($params)
    {
        $this->ref = $params->Ref ?? null;
        $this->description = $params->Description ?? null;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
}
