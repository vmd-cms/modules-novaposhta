<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class TypeItemDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param TypeItemDTO $dto
     */
    public function append(TypeItemDTO $dto)
    {
        $this->collection->add($dto);
    }

}
