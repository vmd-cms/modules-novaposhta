<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\Modules\NovaPoshta\Models\Warehouse;

class WarehouseDTO
{
    /**
     * @var string
     */
    private $siteKey;
    private $description;
    private $descriptionRu;
    private $shortAddress;
    private $shortAddressRu;
    private $phone;
    private $typeOfWarehouse;
    private $ref;
    private $number;
    private $cityRef;
    private $cityDescription;
    private $cityDescriptionRu;
    private $settlementRef;
    private $settlementDescription;
    private $settlementAreaDescription;
    private $settlementRegionsDescription;
    private $settlementTypeDescription;
    private $longitude;
    private $latitude;
    private $postFinance;
    private $bicycleParking;
    private $paymentAccess;
    private $pOSTerminal;
    private $internationalShipping;
    private $selfServiceWorkplacesCount;
    private $totalMaxWeightAllowed;
    private $placeMaxWeightAllowed;
    private $districtCode;
    private $warehouseStatus;
    private $warehouseStatusDate;
    private $categoryOfWarehouse;
    private $direct;

    public function __construct($addressObj)
    {
        if($addressObj instanceof Warehouse){
            $this->mapFromWarehouseObj($addressObj);
        }else{
            $this->mapFromResponseObj($addressObj);
        }
    }

    protected function mapFromWarehouseObj($addressObj){
        $this->siteKey = $addressObj->sity_key ?? null;
        $this->description = $addressObj->info->description ?? null;
        $this->shortAddress = $addressObj->info->short_address ?? null;
        $this->phone = $addressObj->phone ?? null;
        $this->typeOfWarehouse = $addressObj->type_of_warehouse ?? null;
        $this->ref = $addressObj->ref ?? null;
        $this->number = $addressObj->number ?? null;
        $this->cityRef = $addressObj->city_ref ?? null;
        $this->cityDescription = $addressObj->info->city_description ?? null;
        $this->settlementRef = $addressObj->settlement_ref ?? null;
        $this->settlementDescription = $addressObj->info->settlement_description ?? null;
        $this->settlementAreaDescription = $addressObj->info->settlement_area_description ?? null;
        $this->settlementRegionsDescription = $addressObj->info->settlement_regions_description ?? null;
        $this->settlementTypeDescription = $addressObj->info->settlement_type_description ?? null;
        $this->longitude = $addressObj->longitude ?? null;
        $this->latitude = $addressObj->latitude ?? null;
        $this->postFinance = $addressObj->post_finance ?? null;
        $this->bicycleParking = $addressObj->bicycle_parking ?? null;
        $this->paymentAccess = $addressObj->payment_access ?? null;
        $this->pOSTerminal = $addressObj->pos_terminal ?? null;
        $this->internationalShipping = $addressObj->international_shipping ?? null;
        $this->selfServiceWorkplacesCount = $addressObj->self_service_workplaces_count ?? null;
        $this->totalMaxWeightAllowed = $addressObj->total_max_weight_allowed ?? null;
        $this->placeMaxWeightAllowed = $addressObj->place_max_weight_allowed ?? null;
        $this->districtCode = $addressObj->district_code ?? null;
        $this->warehouseStatus = $addressObj->warehouse_status ?? null;
        $this->warehouseStatusDate = $addressObj->warehouse_status_date ?? null;
        $this->categoryOfWarehouse = $addressObj->category_of_warehouse ?? null;
        $this->direct = $addressObj->direct ?? null;
    }

    protected function mapFromResponseObj($addressObj){
        $this->siteKey = $addressObj->SiteKey ?? null;
        $this->description = $addressObj->Description ?? null;
        $this->descriptionRu = $addressObj->DescriptionRu ?? null;
        $this->shortAddress = $addressObj->ShortAddress ?? null;
        $this->shortAddressRu = $addressObj->ShortAddressRu ?? null;
        $this->phone = $addressObj->Phone ?? null;
        $this->typeOfWarehouse = $addressObj->TypeOfWarehouse ?? null;
        $this->ref = $addressObj->Ref ?? null;
        $this->number = $addressObj->Number ?? null;
        $this->cityRef = $addressObj->CityRef ?? null;
        $this->cityDescription = $addressObj->CityDescription ?? null;
        $this->cityDescriptionRu = $addressObj->CityDescriptionRu ?? null;
        $this->settlementRef = $addressObj->SettlementRef ?? null;
        $this->settlementDescription = $addressObj->SettlementDescription ?? null;
        $this->settlementAreaDescription = $addressObj->SettlementAreaDescription ?? null;
        $this->settlementRegionsDescription = $addressObj->SettlementRegionsDescription ?? null;
        $this->settlementTypeDescription = $addressObj->SettlementTypeDescription ?? null;
        $this->longitude = $addressObj->Longitude ?? null;
        $this->latitude = $addressObj->Latitude ?? null;
        $this->postFinance = $addressObj->PostFinance ?? null;
        $this->bicycleParking = $addressObj->BicycleParking ?? null;
        $this->paymentAccess = $addressObj->PaymentAccess ?? null;
        $this->pOSTerminal = $addressObj->POSTerminal ?? null;
        $this->internationalShipping = $addressObj->InternationalShipping ?? null;
        $this->selfServiceWorkplacesCount = $addressObj->SelfServiceWorkplacesCount ?? null;
        $this->totalMaxWeightAllowed = $addressObj->TotalMaxWeightAllowed ?? null;
        $this->placeMaxWeightAllowed = $addressObj->PlaceMaxWeightAllowed ?? null;
        $this->districtCode = $addressObj->DistrictCode ?? null;
        $this->warehouseStatus = $addressObj->WarehouseStatus ?? null;
        $this->warehouseStatusDate = $addressObj->WarehouseStatusDate ?? null;
        $this->categoryOfWarehouse = $addressObj->CategoryOfWarehouse ?? null;
        $this->direct = $addressObj->Direct ?? null;
    }


    /**
     * @return string
     */
    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return null
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }

    /**
     * @return null
     */
    public function getShortAddress()
    {
        return $this->shortAddress;
    }

    /**
     * @return null
     */
    public function getShortAddressRu()
    {
        return $this->shortAddressRu;
    }

    /**
     * @return null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return null
     */
    public function getTypeOfWarehouse()
    {
        return $this->typeOfWarehouse;
    }

    /**
     * @return null
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @return null
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return null
     */
    public function getCityRef()
    {
        return $this->cityRef;
    }

    /**
     * @return null
     */
    public function getCityDescription()
    {
        return $this->cityDescription;
    }

    /**
     * @return null
     */
    public function getCityDescriptionRu()
    {
        return $this->cityDescriptionRu;
    }

    /**
     * @return null
     */
    public function getSettlementRef()
    {
        return $this->settlementRef;
    }

    /**
     * @return null
     */
    public function getSettlementDescription()
    {
        return $this->settlementDescription;
    }

    /**
     * @return null
     */
    public function getSettlementAreaDescription()
    {
        return $this->settlementAreaDescription;
    }

    /**
     * @return null
     */
    public function getSettlementRegionsDescription()
    {
        return $this->settlementRegionsDescription;
    }

    /**
     * @return null
     */
    public function getSettlementTypeDescription()
    {
        return $this->settlementTypeDescription;
    }

    /**
     * @return null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return null
     */
    public function getPostFinance()
    {
        return $this->postFinance;
    }

    /**
     * @return null
     */
    public function getBicycleParking()
    {
        return $this->bicycleParking;
    }

    /**
     * @return null
     */
    public function getPaymentAccess()
    {
        return $this->paymentAccess;
    }

    /**
     * @return null
     */
    public function getPOSTerminal()
    {
        return $this->pOSTerminal;
    }

    /**
     * @return null
     */
    public function getInternationalShipping()
    {
        return $this->internationalShipping;
    }

    /**
     * @return null
     */
    public function getSelfServiceWorkplacesCount()
    {
        return $this->selfServiceWorkplacesCount;
    }

    /**
     * @return null
     */
    public function getTotalMaxWeightAllowed()
    {
        return $this->totalMaxWeightAllowed;
    }

    /**
     * @return null
     */
    public function getPlaceMaxWeightAllowed()
    {
        return $this->placeMaxWeightAllowed;
    }

    /**
     * @return null
     */
    public function getDistrictCode()
    {
        return $this->districtCode;
    }

    /**
     * @return null
     */
    public function getWarehouseStatus()
    {
        return $this->warehouseStatus;
    }

    /**
     * @return null
     */
    public function getWarehouseStatusDate()
    {
        return $this->warehouseStatusDate;
    }

    /**
     * @return null
     */
    public function getCategoryOfWarehouse()
    {
        return $this->categoryOfWarehouse;
    }

    /**
     * @return null
     */
    public function getDirect()
    {
        return $this->direct;
    }


}
