<?php

namespace VmdCms\Modules\NovaPoshta\DTO;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class WarehouseDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param WarehouseDTO $dto
     */
    public function append(WarehouseDTO $dto)
    {
        $this->collection->add($dto);
    }

}
