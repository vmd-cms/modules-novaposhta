<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\Modules\Orders\Models\Order;
use Illuminate\Support\Facades\Lang;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\Modules\NovaPoshta\Exceptions\ValidationException;
use VmdCms\Modules\NovaPoshta\Models\InternetDocument;

class InternetDocumentEntity
{
    public function getCreateInternetDocumentAdminSectionLink(Order $order)
    {
        $internetDocument = InternetDocument::where('order_id',$order->id)->first();
        if(!$internetDocument instanceof InternetDocument){
            $internetDocument = new InternetDocument();
            $internetDocument->order_id = $order->id;
            $internetDocument->moderator_id = AuthEntity::getAuthModeratorId();
            $internetDocument->save();
        }

        return CoreRouter::getEditRoute((new \VmdCms\Modules\NovaPoshta\Sections\InternetDocument())->getSectionSlug(),$internetDocument->id);
    }

    /**
     * @param InternetDocument $internetDocument
     * @throws ValidationException
     */
    public function validateInternetDocumentFields(InternetDocument $internetDocument){

        $attributes = $internetDocument->attributesToArray();

        $requiredFields = [
            "payer_type",
            "payment_method",
            "cargo_type",
            "volume_general",
            "weight",
            "seats_amount",
            "cost",
            "city_sender",
            "sender",
            "sender_address",
            "contact_sender",
            "senders_phone",
            "recipient_city_name",
            "recipient_address_name",
            "recipient_name",
            "recipients_phone",
            "date_time"
        ];

        $missedFields = array_intersect(array_keys($attributes,null,true),$requiredFields);

        if(is_countable($missedFields) && count($missedFields)){
            throw new ValidationException(Lang::get('vmd_cms::dashboard.not_set_all_required'));
        }

        if(preg_match('/[A-Za-z]/u', $attributes['recipient_name'])){
            throw new ValidationException(Lang::get('Имя получателя не должно быть на латинице'));
        }

    }
}
