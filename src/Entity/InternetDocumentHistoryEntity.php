<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\Modules\NovaPoshta\DTO\InternetDocumentDTO;
use VmdCms\Modules\NovaPoshta\Models\InternetDocument;
use VmdCms\Modules\NovaPoshta\Models\InternetDocumentHistory;

class InternetDocumentHistoryEntity
{
    public function storeHistory(InternetDocument $internetDocument)
    {
        $dto = new InternetDocumentDTO($internetDocument);

        $history = new InternetDocumentHistory();
        $history->moderator_id = AuthEntity::getAuthModeratorId();
        $history->nova_poshta_internet_document_id = $internetDocument->id;
        $history->nova_poshta_internet_document_ref = $internetDocument->ref;
        $history->dto = json_encode($dto->toArray());
        $history->cost_on_site = $internetDocument->cost_on_site;
        $history->estimated_delivery_date = $internetDocument->estimated_delivery_date;
        $history->int_doc_number = $internetDocument->int_doc_number;
        $history->type_document = $internetDocument->type_document;
        $history->save();
    }

}
