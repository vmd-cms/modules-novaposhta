<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Config;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;

class NovaPoshtaApi
{
    protected $key;
    protected $url;
    protected $lang;

    public function __construct()
    {
        $this->key = Config::get('services.nova_poshta.key');
        $this->url = 'https://api.novaposhta.ua/v2.0/json/';
        $this->lang = Languages::getInstance()->getCurrentLocaleActive();
    }

    protected function request($options = [])
    {
        try {

            $options['apiKey'] = $this->key;

            $client = new Client();
            $request = $client->post($this->url, [
                RequestOptions::JSON => $options
            ]);

            $response = json_decode($request->getBody()->getContents());

            $success = $response ? $response->success : false;
            $data = $response ? $response->data : [];
            $errors = $response ? $response->errors : [];
            $errorCodes = $response ? $response->errorCodes : [];
        }
        catch (\Exception $exception){
            $success = false;
            $data = null;
            $errors = [];
            $errorCodes = [];
        }

        return [
            'success' => $success,
            'errors' => $errors,
            'error_codes' => $errorCodes,
            'data' => $data
        ];
    }


}
