<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\Modules\NovaPoshta\DTO\Counterparty\CounterpartyContactDTO;
use VmdCms\Modules\NovaPoshta\DTO\Counterparty\CounterpartyContactDTOCollection;
use VmdCms\Modules\NovaPoshta\DTO\Counterparty\CounterpartyDTO;
use VmdCms\Modules\NovaPoshta\DTO\Counterparty\CounterpartyDTOCollection;
use VmdCms\Modules\NovaPoshta\DTO\TypeItemDTO;
use VmdCms\Modules\NovaPoshta\DTO\TypeItemDTOCollection;
use VmdCms\Modules\NovaPoshta\Models\Settings\Counterparty;

class NovaPoshtaCounterparties extends NovaPoshtaApi
{
    public function createCounterparty(Counterparty $model)
    {
        $body = [
            "modelName" =>"Counterparty",
            "calledMethod" => "save",
            "methodProperties"=> [
                "FirstName" => $model->first_name,
                "MiddleName" => $model->middle_name,
                "LastName" => $model->last_name,
                "Phone" => $model->phone,
                "Email" => $model->email,
                "CounterpartyType" => $model->counterparty_type,
                "CounterpartyProperty" => $model->counterparty_property
            ]
        ];

        $result = $this->request($body);

        $item = $result['data'][0] ?? null;

        return $item ? $item->Ref : null;
    }

    public function updateCounterparty(Counterparty $model)
    {
        $body = [
            "modelName" =>"Counterparty",
            "calledMethod" => "update",
            "methodProperties"=> [
                "Ref" => $model->ref,
                "FirstName" => $model->first_name,
                "MiddleName" => $model->middle_name,
                "LastName" => $model->last_name,
                "Phone" => $model->phone,
                "Email" => $model->email,
                "CounterpartyType" => $model->counterparty_type,
                "CounterpartyProperty" => $model->counterparty_property
            ]
        ];

        $result = $this->request($body);
        $item = $result['data'][0] ?? null;
        return $item ? $item->Ref : null;
    }

    /**
     * @param string $type
     * @return CounterpartyDTOCollection
     */
    public function getListCounterparties(string $type = 'Sender'): CounterpartyDTOCollection
    {

        $collection = new CounterpartyDTOCollection();

        $body = [
            "modelName" =>"Counterparty",
            "calledMethod" => "getCounterparties",
            "methodProperties" => [
                "CounterpartyProperty"=> $type,
                "Page" => "1"
            ]
        ];

        $result = $this->request($body);
        $items =  $result['data'] ?? null;

        if(is_countable($items)){
            foreach ($items as $item){
                $collection->append(new CounterpartyDTO($item));
            }
        }
        return $collection;
    }

    /**
     * @param string $conterpartyRef
     * @return CounterpartyContactDTOCollection
     */
    public function getCounterpartyListContacts(string $conterpartyRef): CounterpartyContactDTOCollection
    {

        $collection = new CounterpartyContactDTOCollection();

        $body = [
            "modelName" =>"Counterparty",
            "calledMethod" => "getCounterpartyContactPersons",
            "methodProperties" => [
                "Ref"   => $conterpartyRef,
                "Page"  => "1"
            ]
        ];

        $result = $this->request($body);
        $items =  $result['data'] ?? null;

        if(is_countable($items)){
            foreach ($items as $item){
                $collection->append(new CounterpartyContactDTO($item));
            }
        }
        return $collection;
    }

    /**
     * @param string $conterpartyRef
     * @param string|null $cityRef
     * @param string $type
     * @return TypeItemDTOCollection
     */
    public function getCounterpartyAddresses(string $conterpartyRef, string $cityRef = null, string $type = 'Sender'): TypeItemDTOCollection
    {
        $collection = new TypeItemDTOCollection();

        if(!in_array($type,['Sender','Recipient'])){
            $type = 'Sender';
        }

        $properties = [
            "Ref"   => $conterpartyRef,
            "CounterpartyProperty" => $type,
        ];

        if(!empty($cityRef)){
            $properties['CityRef'] = $cityRef;
        }

        $body = [
            "modelName" =>"Counterparty",
            "calledMethod" => "getCounterpartyAddresses",
            "methodProperties" => $properties
        ];

        $result = $this->request($body);
        $items =  $result['data'] ?? null;

        if(is_countable($items)){
            foreach ($items as $item){
                $collection->append(new TypeItemDTO($item));
            }
        }
        return $collection;
    }

    /**
     * @param string $conterpartyRef
     * @return TypeItemDTOCollection
     */
    public function getCounterpartyOptions(string $conterpartyRef): TypeItemDTOCollection
    {
        $collection = new TypeItemDTOCollection();

        $body = [
            "modelName" =>"Counterparty",
            "calledMethod" => "getCounterpartyOptions",
            "methodProperties" => [
                "Ref"   => $conterpartyRef
            ]
        ];

        $result = $this->request($body);
        $items =  $result['data'] ?? null;

        if(is_countable($items)){
            foreach ($items as $item){
                $collection->append(new TypeItemDTO($item));
            }
        }
        return $collection;
    }
}
