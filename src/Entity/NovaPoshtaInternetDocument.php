<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\Modules\NovaPoshta\DTO\InternetDocumentDTO;
use VmdCms\Modules\NovaPoshta\Exceptions\AlreadyDeletedException;
use VmdCms\Modules\NovaPoshta\Exceptions\ApiException;
use VmdCms\Modules\NovaPoshta\Models\InternetDocument;
use VmdCms\Modules\NovaPoshta\Models\Settings\ErrorCode;

class NovaPoshtaInternetDocument extends NovaPoshtaApi
{
    /**
     * @param InternetDocumentDTO $dto
     * @return mixed|null
     * @throws ApiException
     */
    public function createOrUpdateInternetDocument(InternetDocumentDTO $dto)
    {
        $body = [
            "modelName" =>"InternetDocument",
            "calledMethod" => $dto->getRef() ? "update" : "save",
            "methodProperties"=> $dto->toArray()
        ];

        $result = $this->request($body);

        if(!$result['success']){

            $messages = [];
            $errorCodes = $result['error_codes'] ?? [];

            foreach ($errorCodes as $errorCode){
                $messages[] = ErrorCode::getErrorByCode($errorCode);
            }

            if(!count($messages)){
                $messages = $result['errors'] ?? [];
            }

            throw new ApiException(implode('</br>',$messages),409);
        }

        $item = $result['data'][0] ?? null;

        return $item;
    }

    /**
     * @param InternetDocumentDTO $dto
     * @return mixed|null
     * @throws AlreadyDeletedException
     * @throws ApiException
     */
    public function deleteInternetDocument(InternetDocumentDTO $dto)
    {
        $ref = $dto->getRef();

        if(empty($ref)){
            throw new ApiException('Empty ref');
        }

        $body = [
            "modelName" =>"InternetDocument",
            "calledMethod" => "delete",
            "methodProperties"=> [
                'DocumentRefs' => $ref
            ]
        ];

        $result = $this->request($body);

        if(!$result['success']){

            if(isset($result['errors'],$result['errors']->$ref)){
                throw new AlreadyDeletedException($result['errors']->$ref);
            }

            $messages = [];
            $errorCodes = $result['error_codes'] ?? [];

            foreach ($errorCodes as $errorCode){
                $messages[] = ErrorCode::getErrorByCode($errorCode);
            }

            if(!count($messages)){
                $messages = $result['errors'] ?? [];
            }

            throw new ApiException(implode('</br>',$messages),409);
        }

        $item = $result['data'][0] ?? null;

        return $item;
    }

    public function getPrintInternetDocument(InternetDocument $internetDocument)
    {
        try {
            $url = 'https://my.novaposhta.ua/orders/printDocument/orders[]/' . $internetDocument->int_doc_number . '/type/html/apiKey/' . $this->key;

            $content = file_get_contents($url);
        }
        catch (\Exception $exception){
            $content = '';
        }
        return $content;
    }
}
