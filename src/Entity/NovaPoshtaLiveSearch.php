<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\Modules\NovaPoshta\DTO\AddressDTO;
use VmdCms\Modules\NovaPoshta\DTO\AddressDTOCollection;
use VmdCms\Modules\NovaPoshta\DTO\WarehouseDTO;
use VmdCms\Modules\NovaPoshta\DTO\WarehouseDTOCollection;
use VmdCms\Modules\NovaPoshta\Exceptions\ApiException;

class NovaPoshtaLiveSearch extends NovaPoshtaApi
{
    /**
     * @param string $needle
     * @param int $limit
     * @return AddressDTOCollection
     */
    public function getAddresses(string $needle, $limit = 10): AddressDTOCollection
    {
        $collection = new AddressDTOCollection();
        $body = [
            "modelName" =>"Address",
            "calledMethod" => "searchSettlements",
            "methodProperties"=> [
                "CityName"=> $needle,
                "Limit"=> $limit
            ]
        ];

        $result = $this->request($body);
        $data = $result['data'][0] ?? null;
        if(isset($data,$data->Addresses) && is_countable($data->Addresses)){
            foreach ($data->Addresses as $address){
                $collection->append(new AddressDTO($address));
            }
        }
        return $collection;
    }

    /**
     * @param string $cityRef
     * @param int $limit
     * @param int $page
     * @return WarehouseDTOCollection
     * @throws ApiException
     */
    public function getWarehouses(string $cityRef, $limit = 10, $page = 1): WarehouseDTOCollection
    {
        $collection = new WarehouseDTOCollection();

        $body = [
            "modelName" =>"AddressGeneral",
            "calledMethod" => "getWarehouses",
            "methodProperties"=> [
                "Language" => $this->lang,
                "CityRef" => $cityRef,
                "Limit" => $limit,
                "Page" => $page
            ]
        ];

        $result = $this->request($body);
        $data = $result['data'];
        if(is_countable($data)){
            foreach ($data as $item){
                $collection->append(new WarehouseDTO($item));
            }
        }
        return $collection;
    }
}
