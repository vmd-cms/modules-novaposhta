<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\Modules\NovaPoshta\DTO\AddressDTO;
use VmdCms\Modules\NovaPoshta\DTO\AddressDTOCollection;
use VmdCms\Modules\NovaPoshta\DTO\WarehouseDTO;
use VmdCms\Modules\NovaPoshta\DTO\WarehouseDTOCollection;
use VmdCms\Modules\NovaPoshta\Exceptions\ApiException;
use VmdCms\Modules\NovaPoshta\Models\City;
use VmdCms\Modules\NovaPoshta\Models\Warehouse;

class NovaPoshtaSearch
{
    /**
     * @param string $needle
     * @param int $limit
     * @return AddressDTOCollection
     */
    public function getAddresses(string $needle, $limit = 10): AddressDTOCollection
    {
        $collection = new AddressDTOCollection();
        try {
            $cities = City::whereHas('info',function ($q) use ($needle){
                $q->where('description','like','%' . $needle . '%');
            })->with('info')->orderBy('settlement_type')->limit($limit)->get();

        }catch (\Exception $exception){
            $cities = null;
        }

        if(is_countable($cities)){
            foreach ($cities as $city){
                $collection->append(new AddressDTO($city));
            }
        }
        return $collection;
    }

    /**
     * @param string $cityRef
     * @param int $limit
     * @param int $page
     * @return WarehouseDTOCollection
     * @throws ApiException
     */
    public function getWarehouses(string $cityRef, $limit = 10, $page = 1): WarehouseDTOCollection
    {
        $collection = new WarehouseDTOCollection();

        try {
            $skip = $page > 1 ? $limit * ($page - 1) : 0;
            $warehouses = Warehouse::where('city_ref',$cityRef)->limit($limit)->skip($skip)->get();

        }catch (\Exception $exception){
            $warehouses = null;
        }

        if(is_countable($warehouses)){
            foreach ($warehouses as $item){
                $collection->append(new WarehouseDTO($item));
            }
        }
        return $collection;
    }
}
