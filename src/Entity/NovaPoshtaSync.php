<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\Modules\NovaPoshta\Models\City;
use VmdCms\Modules\NovaPoshta\Models\CityInfo;
use VmdCms\Modules\NovaPoshta\Models\Warehouse;
use VmdCms\Modules\NovaPoshta\Models\WarehouseInfo;

class NovaPoshtaSync extends NovaPoshtaApi
{
    protected $hasUkrainian;
    protected $hasRussian;

    public function __construct()
    {
        parent::__construct();
        $languagesService = Languages::getInstance();
        $this->hasUkrainian = $languagesService->hasLanguage('ua');
        $this->hasRussian = $languagesService->hasLanguage('ru');
    }

    public function syncCities()
    {

        $body = [
            "modelName" =>"AddressGeneral",
            "calledMethod" => "getCities"
        ];

        $result = $this->request($body);
        $data = $result['data'];
        if(isset($data) && is_countable($data)){
            foreach ($data as $cityData){
                $model = City::where('ref',$cityData->Ref)->first();
                if(!$model instanceof City){
                    $model = new City();
                }
                $this->syncCityModelFromData($model,$cityData);
            }
        }
        return true;
    }

    public function syncWarehouses()
    {

        ini_set('max_execution_time',600);

        $body = [
            "modelName" =>"AddressGeneral",
            "calledMethod" => "getWarehouses"
        ];

        $result = $this->request($body);
        $data = $result['data'];
        if(isset($data) && is_countable($data)){
            foreach ($data as $warehouseData){
                $model = Warehouse::where('ref',$warehouseData->Ref)->first();
                if(!$model instanceof Warehouse){
                    $model = new Warehouse();
                }
                $this->syncWarehouseModelFromData($model,$warehouseData);
            }
        }
        return true;
    }

    protected function syncCityModelFromData(City $model,$data)
    {
        try {

            DB::beginTransaction();

            $model->ref = $data->Ref ?? null;
            $model->delivery_1 = $data->Delivery1 ?? null;
            $model->delivery_2 = $data->Delivery2 ?? null;
            $model->delivery_3 = $data->Delivery3 ?? null;
            $model->delivery_4 = $data->Delivery4 ?? null;
            $model->delivery_5 = $data->Delivery5 ?? null;
            $model->delivery_6 = $data->Delivery6 ?? null;
            $model->delivery_7 = $data->Delivery7 ?? null;
            $model->area = $data->Area ?? null;
            $model->settlement_type = $data->SettlementType ?? null;
            $model->is_branch = $data->IsBranch ?? null;
            $model->prevent_entry_new_streets_user = $data->PreventEntryNewStreetsUser ?? null;
            $model->conglomerates = isset($data->Conglomerates) ? (is_array($data->Conglomerates) ? json_encode($data->Conglomerates) : $data->Conglomerates) : null;
            $model->city_id = $data->CityID ?? null;
            $model->special_cash_check = $data->SpecialCashCheck ?? null;
            $model->save();

            if($this->hasUkrainian){
                Languages::getInstance()->setLocale('ua');
                $modelInfo = CityInfo::where('nova_poshta_cities_id',$model->id)->where('lang_key','ua')->first();
                if(!$modelInfo instanceof CityInfo){
                    $modelInfo = new CityInfo();
                    $modelInfo->nova_poshta_cities_id = $model->id;
                    $modelInfo->lang_key = 'ua';
                }
                $modelInfo->description = $data->Description ?? null;
                $modelInfo->settlement_type_description = $data->SettlementTypeDescription ?? null;
                $modelInfo->area_description = $data->AreaDescription ?? null;
                $modelInfo->save();
            }

            if($this->hasRussian){
                Languages::getInstance()->setLocale('ru');
                $modelInfo = CityInfo::where('nova_poshta_cities_id',$model->id)->where('lang_key','ru')->first();
                if(!$modelInfo instanceof CityInfo){
                    $modelInfo = new CityInfo();
                    $modelInfo->nova_poshta_cities_id = $model->id;
                    $modelInfo->lang_key = 'ru';
                }
                $modelInfo->description = $data->DescriptionRu ?? null;
                $modelInfo->settlement_type_description = $data->SettlementTypeDescriptionRu ?? null;
                $modelInfo->area_description = $data->AreaDescriptionRu ?? null;
                $modelInfo->save();
            }

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
        }
    }

    protected function syncWarehouseModelFromData(Warehouse $model,$data)
    {
        try {
            DB::beginTransaction();

            $model->ref = $data->Ref ?? null;
            $model->sity_key = $data->SityKey ?? null;
            $model->phone = $data->Phone ?? null;
            $model->type_of_warehouse = $data->TypeOfWarehouse ?? null;
            $model->ref = $data->Ref ?? null;
            $model->number = $data->Number ?? null;
            $model->city_ref = $data->CityRef ?? null;
            $model->settlement_ref = $data->SettlementRef ?? null;
            $model->longitude = $data->Longitude ?? null;
            $model->latitude = $data->Latitude ?? null;
            $model->post_finance = $data->PostFinance ?? null;
            $model->bicycle_parking = $data->BicycleParking ?? null;
            $model->payment_access = $data->PaymentAccess ?? null;
            $model->pos_terminal = $data->PosTerminal ?? null;
            $model->international_shipping = $data->InternationalShipping ?? null;
            $model->self_service_workplaces_count = $data->SelfServiceWorkplacesCount ?? null;
            $model->total_max_weight_allowed = $data->TotalMaxWeightAllowed ?? null;
            $model->place_max_weight_allowed = $data->PlaceMaxWeightAllowed ?? null;
            $model->sending_limitations_on_dimensions = json_encode($data->SendingLimitationsOnDimensions ?? null);
            $model->receiving_limitations_on_dimensions = json_encode($data->ReceivingLimitationsOnDimensions ?? null);
            $model->reception = json_encode($data->Reception ?? null);
            $model->delivery = json_encode($data->Delivery ?? null);
            $model->schedule = json_encode($data->Schedule ?? null);
            $model->district_code = $data->DistrictCode ?? null;
            $model->warehouse_status = $data->WarehouseStatus ?? null;
            $model->warehouse_status_date = $data->WarehouseStatusDate ?? null;
            $model->category_of_warehouse = $data->CategoryOfWarehouse ?? null;
            $model->direct = $data->Direct ?? null;
            $model->region_city = $data->RegionCity ?? null;
            $model->save();

            if($this->hasUkrainian){
                Languages::getInstance()->setLocale('ua');
                $modelInfo = WarehouseInfo::where('nova_poshta_warehouses_id',$model->id)->where('lang_key','ua')->first();
                if(!$modelInfo instanceof WarehouseInfo){
                    $modelInfo = new WarehouseInfo();
                    $modelInfo->nova_poshta_warehouses_id = $model->id;
                    $modelInfo->lang_key = 'ua';
                }
                $modelInfo->description = $data->Description ?? null;
                $modelInfo->short_address = $data->ShortAddress ?? null;
                $modelInfo->city_description = $data->CityDescription ?? null;
                $modelInfo->settlement_description = $data->SettlementDescription ?? null;
                $modelInfo->settlement_area_description = $data->SettlementAreaDescription ?? null;
                $modelInfo->settlement_regions_description = $data->SettlementRegionsDescription ?? null;
                $modelInfo->settlement_type_description = $data->SettlementTypeDescription ?? null;
                $modelInfo->save();
            }

            if($this->hasRussian){
                Languages::getInstance()->setLocale('ru');
                $modelInfo = WarehouseInfo::where('nova_poshta_warehouses_id',$model->id)->where('lang_key','ru')->first();
                if(!$modelInfo instanceof WarehouseInfo){
                    $modelInfo = new WarehouseInfo();
                    $modelInfo->nova_poshta_warehouses_id = $model->id;
                    $modelInfo->lang_key = 'ru';
                }
                $modelInfo->description = $data->DescriptionRu ?? null;
                $modelInfo->short_address = $data->ShortAddressRu ?? null;
                $modelInfo->city_description = $data->CityDescriptionRu ?? null;
                $modelInfo->settlement_description = $data->SettlementDescription ?? null;
                $modelInfo->settlement_area_description = $data->SettlementAreaDescription ?? null;
                $modelInfo->settlement_regions_description = $data->SettlementRegionsDescription ?? null;
                $modelInfo->settlement_type_description = $data->SettlementTypeDescription ?? null;
                $modelInfo->save();
            }

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
        }
    }


}
