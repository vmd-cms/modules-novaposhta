<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\NovaPoshta\DTO\ErrorCodeDTO;
use VmdCms\Modules\NovaPoshta\DTO\ErrorCodeDTOCollection;
use VmdCms\Modules\NovaPoshta\DTO\TypeItemDTO;
use VmdCms\Modules\NovaPoshta\DTO\TypeItemDTOCollection;

class NovaPoshtaTypeList extends NovaPoshtaApi
{
    /**
     * @return TypeItemDTOCollection
     */
    public function getTypesOfPayers()
    {
        return $this->getTypes('getTypesOfPayers');
    }

    /**
     * @return TypeItemDTOCollection
     */
    public function getPaymentForms()
    {
        return $this->getTypes('getPaymentForms');
    }

    /**
     * @return TypeItemDTOCollection
     */
    public function getCargoTypes()
    {
        return $this->getTypes('getCargoTypes');
    }

    /**
     * @return TypeItemDTOCollection
     */
    public function getServiceTypes()
    {
        return $this->getTypes('getServiceTypes');
    }

    /**
     * @return TypeItemDTOCollection
     */
    protected function getTypes(string $calledMethod)
    {
        $typeItems = new TypeItemDTOCollection();

        $body = [
            "modelName" =>"Common",
            "calledMethod" => $calledMethod,
            "methodProperties"=> null
        ];

        $result = $this->request($body);

        $items = $result['data'] ?? null;
        if(is_countable($items)){
            foreach ($items as $item){
                $typeItems->append(new TypeItemDTO($item));
            }
        }

        return $typeItems;
    }

    /**
     * @return ErrorCodeDTOCollection
     */
    public function getErrorCodes()
    {
        $errorItems = new ErrorCodeDTOCollection();

        $body = [
            "modelName" =>"CommonGeneral",
            "calledMethod" => 'getMessageCodeText',
            "methodProperties"=> null
        ];

        $result = $this->request($body);

        $items = $result['data'] ?? null;
        if(is_countable($items)){
            foreach ($items as $item){
                $errorItems->append(new ErrorCodeDTO($item));
            }
        }

        return $errorItems;
    }



}
