<?php

namespace VmdCms\Modules\NovaPoshta\Entity;

use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\Modules\NovaPoshta\DTO\Counterparty\CounterpartyContactDTO;
use VmdCms\Modules\NovaPoshta\DTO\Counterparty\CounterpartyDTO;
use VmdCms\Modules\NovaPoshta\DTO\ErrorCodeDTO;
use VmdCms\Modules\NovaPoshta\DTO\TypeItemDTO;
use VmdCms\Modules\NovaPoshta\Exceptions\SyncException;
use VmdCms\Modules\NovaPoshta\Models\Settings\CargoType;
use VmdCms\Modules\NovaPoshta\Models\Settings\CounterpartySenderContact;
use VmdCms\Modules\NovaPoshta\Models\Settings\ErrorCode;
use VmdCms\Modules\NovaPoshta\Models\Settings\PaymentForm;
use VmdCms\Modules\NovaPoshta\Models\Settings\ServiceType;
use VmdCms\Modules\NovaPoshta\Models\Settings\TypeOfPayer;

class SettingsSyncEntity
{
    public function syncTypeOfPayer()
    {
        $this->syncTypes(TypeOfPayer::class,'getTypesOfPayers');
    }

    public function syncPaymentForm()
    {
        $this->syncTypes(PaymentForm::class,'getPaymentForms');
    }

    public function syncCargoTypes()
    {
        $this->syncTypes(CargoType::class,'getCargoTypes');
    }

    public function syncServiceTypes()
    {
        $this->syncTypes(ServiceType::class,'getServiceTypes');
    }

    public function syncErrorCodes()
    {
        try {

            DB::beginTransaction();

            ErrorCode::whereNotNull('id')->delete();

            $apiItems = (new NovaPoshtaTypeList())->getErrorCodes()->getItems();

            if(is_countable($apiItems)){
                foreach ($apiItems as $apiItem){
                    if(!$apiItem instanceof ErrorCodeDTO){
                        continue;
                    }

                    $item = new ErrorCode();
                    $item->title = $apiItem->getCode();
                    $item->error = $apiItem->getError();
                    $item->description_ru = $apiItem->getDescriptionRu();
                    $item->description_ua = $apiItem->getDescriptionUa();
                    $item->save();
                }
            }

            DB::commit();

        }catch (\Exception $exception){
            DB::rollBack();
            throw new SyncException();
        }
    }

    /**
     * @return CounterpartySenderContact
     * @throws SyncException
     */
    public function getCounterpartySenderContact(): CounterpartySenderContact
    {
        $apiEntity = new NovaPoshtaCounterparties();

        $apiCounterparties = $apiEntity->getListCounterparties()->getItems();

        $apiCounterparty = !empty($apiCounterparties) ? $apiCounterparties->first() : null;

        if(!$apiCounterparty instanceof CounterpartyDTO){
            throw new SyncException('Empty cabinet counterparty data');
        }

        $apiContacts = $apiEntity->getCounterpartyListContacts($apiCounterparty->getRef())->getItems();
        $apiContact = !empty($apiContacts) ? $apiContacts->first() : null;

        if(!$apiContact instanceof CounterpartyContactDTO){
            throw new SyncException('Empty cabinet counterparty data');
        }

        $counterparty = CounterpartySenderContact::first();
        if(!$counterparty instanceof CounterpartySenderContact){
            $counterparty = new CounterpartySenderContact();
        }

        try {
            $counterparty->counterparty_ref = $apiCounterparty->getRef();
            $counterparty->contact_ref = $apiContact->getRef();
            $counterparty->first_name = $apiContact->getFirstName();
            $counterparty->middle_name = $apiContact->getMiddleName();
            $counterparty->last_name = $apiContact->getLastName();
            $counterparty->phones = $apiContact->getPhones();
            $counterparty->email = $apiContact->getEmail();
            $counterparty->description = $apiContact->getDescription();

            $counterparty->save();
        }catch (\Exception $exception){
            throw new SyncException('Error store CounterpartySenderContact');
        }

        return $counterparty;
    }

    protected function syncTypes(string $modelClass, string $syncMethod){

        try {

            if(!class_exists($modelClass)){
                throw new SyncException();
            }

            DB::beginTransaction();

            $modelClass::whereNotNull('id')->delete();

            $apiItems = (new NovaPoshtaTypeList())->$syncMethod()->getItems();

            if(is_countable($apiItems)){
                foreach ($apiItems as $apiItem){
                    if(!$apiItem instanceof TypeItemDTO){
                        continue;
                    }

                    $item = new $modelClass();
                    $item->ref = $apiItem->getRef();
                    $item->description = $apiItem->getDescription();
                    $item->save();
                }
            }

            DB::commit();

        }catch (\Exception $exception){
            DB::rollBack();
            throw new SyncException();
        }
    }
}
