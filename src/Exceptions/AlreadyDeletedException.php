<?php

namespace VmdCms\Modules\NovaPoshta\Exceptions;

use VmdCms\CoreCms\Exceptions\CoreException;

class AlreadyDeletedException extends CoreException
{

}
