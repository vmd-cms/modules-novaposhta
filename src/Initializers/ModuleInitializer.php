<?php

namespace VmdCms\Modules\NovaPoshta\Initializers;;

use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'nova_poshta';
    const ALIAS = 'NovaPoshta';

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }
}
