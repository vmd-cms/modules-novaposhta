<?php

namespace VmdCms\Modules\NovaPoshta\Models;

use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\HasInfo;

class City extends CmsModel implements HasInfoInterface
{
    use HasInfo;

    public static function table(): string
    {
        return 'nova_poshta_cities';
    }
}
