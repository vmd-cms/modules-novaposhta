<?php

namespace VmdCms\Modules\NovaPoshta\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class CityInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'nova_poshta_cities_info';
    }
}
