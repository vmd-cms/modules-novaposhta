<?php

namespace VmdCms\Modules\NovaPoshta\Models;

use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\JsonData;

class InternetDocument extends CmsModel
{
    use JsonData;

    public static function table(): string
    {
        return 'nova_poshta_internet_document';
    }
}
