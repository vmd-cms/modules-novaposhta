<?php

namespace VmdCms\Modules\NovaPoshta\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;

class NovaPoshtaSettings extends CmsModel implements OrderableInterface,ActivableInterface
{
    use Orderable, Activable, JsonData;

    const KEY = null;

    public static function table(): string
    {
        return 'nova_poshta_settings';
    }

    public static function getModelKey(){
        return static::KEY;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
            static::saving(function (NovaPoshtaSettings $model){
                $model->key = static::getModelKey();
            });
        }

    }

}
