<?php

namespace VmdCms\Modules\NovaPoshta\Models\Settings;

use VmdCms\Modules\NovaPoshta\Models\NovaPoshtaSettings;

class Counterparty extends NovaPoshtaSettings
{
    const KEY = 'counterparty';

    public function getJsonDataFieldsArr() : array
    {
        return [
            "ref",
            "first_name",
            "middle_name",
            "last_name",
            "phone",
            "email",
            "counterparty_type",
            "counterparty_property"
        ];
    }
}
