<?php

namespace VmdCms\Modules\NovaPoshta\Models\Settings;

use VmdCms\Modules\NovaPoshta\Models\NovaPoshtaSettings;

class CounterpartySenderContact extends NovaPoshtaSettings
{
    const KEY = 'counterparty_sender_contact';

    public function getFullNameAttribute(){
        $fullName = [];
        if(!empty($this->first_name)){
            $fullName[] = $this->first_name;
        }
        if(!empty($this->middle_name)){
            $fullName[] = $this->middle_name;
        }
        if(!empty($this->last_name)){
            $fullName[] = $this->last_name;
        }
        return implode(' ',$fullName);
    }

    public function getJsonDataFieldsArr() : array
    {
        return [
            "counterparty_ref",
            "contact_ref",
            "first_name",
            "middle_name",
            "last_name",
            "phones",
            "email",
            "description",
            "city_ref",
            "city_title",
            "city_delivery_id",
            "warehouse_ref",
            "warehouse_title"
        ];
    }
}
