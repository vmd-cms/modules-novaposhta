<?php

namespace VmdCms\Modules\NovaPoshta\Models\Settings;

use VmdCms\Modules\NovaPoshta\Models\NovaPoshtaSettings;

class ErrorCode extends NovaPoshtaSettings
{
    const KEY = 'error_code';

    public function getJsonDataFieldsArr() : array
    {
        return [
            "error",
            "description_ru",
            "description_ua"
        ];
    }

    public static function getErrorByCode(string $code): ?string
    {
        $model = self::where('title',$code)->first();
        return $model instanceof self ? $model->description_ru : null;
    }

}
