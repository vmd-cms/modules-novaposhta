<?php

namespace VmdCms\Modules\NovaPoshta\Models\Settings;

use VmdCms\Modules\NovaPoshta\Models\NovaPoshtaSettings;

abstract class TypeItem extends NovaPoshtaSettings
{
    public function getJsonDataFieldsArr() : array
    {
        return [
            "ref",
            "description"
        ];
    }

    public static function getEnums(){
        $enumValues = [];
        $items = static::get();
        if(is_countable($items)){
            foreach ($items as $item){
                $enumValues[$item->ref] = $item->description;
            }
        }
        return $enumValues;
    }
}
