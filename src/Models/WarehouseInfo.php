<?php

namespace VmdCms\Modules\NovaPoshta\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class WarehouseInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'nova_poshta_warehouses_info';
    }
}
