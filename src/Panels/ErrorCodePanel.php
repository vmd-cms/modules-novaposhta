<?php

namespace VmdCms\Modules\NovaPoshta\Panels;

use VmdCms\Modules\NovaPoshta\Sections\Catalogs\AbstractNovaPoshtaSettings;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaErrorCode;

class ErrorCodePanel extends AbstractSettingsPanel
{
    protected function getSectionModel(): AbstractNovaPoshtaSettings
    {
        return new NovaPoshtaErrorCode();
    }
}
