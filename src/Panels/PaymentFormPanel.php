<?php

namespace VmdCms\Modules\NovaPoshta\Panels;

use VmdCms\Modules\NovaPoshta\Sections\Catalogs\AbstractNovaPoshtaSettings;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaPaymentForm;

class PaymentFormPanel extends AbstractSettingsPanel
{
    protected function getSectionModel(): AbstractNovaPoshtaSettings
    {
        return new NovaPoshtaPaymentForm();
    }
}
