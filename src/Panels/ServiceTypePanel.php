<?php

namespace VmdCms\Modules\NovaPoshta\Panels;

use VmdCms\Modules\NovaPoshta\Sections\Catalogs\AbstractNovaPoshtaSettings;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaServiceType;

class ServiceTypePanel extends AbstractSettingsPanel
{
    protected function getSectionModel(): AbstractNovaPoshtaSettings
    {
        return new NovaPoshtaServiceType();
    }
}
