<?php

namespace VmdCms\Modules\NovaPoshta\Panels;

use VmdCms\Modules\NovaPoshta\Sections\Catalogs\AbstractNovaPoshtaSettings;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaTypeOfPayer;

class TypeOfPayerPanel extends AbstractSettingsPanel
{

    protected function getSectionModel(): AbstractNovaPoshtaSettings
    {
        return new NovaPoshtaTypeOfPayer();
    }
}
