<?php

namespace VmdCms\Modules\NovaPoshta\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;
use VmdCms\CoreCms\Dashboard\Display\Panels\AbstractSectionPanel;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaWarehouse;

class WarehousePanel extends AbstractSectionPanel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->viewPath = 'vmd_cms::admin.views.sections.display.panels.panel_wrapper';
    }

    public function getCustomSectionPanel(): SectionPanelInterface
    {
        return (new NovaPoshtaWarehouse())->getDisplayPanel();
    }

    protected function getPreparedData(): array
    {
        return array_merge(parent::getPreparedData(),['panel'=>$this->getCustomSectionPanel()->render()]);
    }

}
