<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCustomButton;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;

abstract class AbstractNovaPoshtaSettings extends CmsSection
{
    /**
     * @return DisplayInterface
     */
    public function display()
    {
        return Display::dataTable([
            Column::text('ref','Идинтификатор'),
            Column::text('description','Описание'),
        ])->setSearchable(true);
    }

    public function getDisplayPanel()
    {
        return $this->display()
            ->setItemsPerPage(10)
            ->setShowActionColumn(false)
            ->setSection($this)
            ->setButtons([
                (new FormCustomButton('Синхронизировать'))
                    ->setRoutePath(route(CoreRouter::ROUTE_SERVICE_DATA,[
                        'sectionSlug' => $this->getSectionSlug(),
                        'sectionMethod' => 'syncSettings',
                        'id' => null
                    ]))
                    ->setGroupKey(FormButton::GROUP_ACTION_HEADER)
                    ->setAction(FormButton::ACTION_SERVICE_PATH)
                    ->setIconClass('icon-descargar')])
            ->getSectionPanel();
    }

    public abstract function syncSettings();

    public function isCreatable(): bool
    {
        return false;
    }

    public function isViewable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return false;
    }
}
