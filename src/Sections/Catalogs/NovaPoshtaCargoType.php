<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Models\Settings\CargoType;

class NovaPoshtaCargoType extends AbstractNovaPoshtaSettings
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_cargo_type';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Виды груза";
    }

    public function getCmsModelClass(): string
    {
        return CargoType::class;
    }

    public function syncSettings()
    {
        try {
            (new SettingsSyncEntity())->syncCargoTypes();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
