<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Models\Settings\ErrorCode;

class NovaPoshtaErrorCode extends AbstractNovaPoshtaSettings
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_error_code';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Коды ошибок";
    }

    public function getCmsModelClass(): string
    {
        return ErrorCode::class;
    }
    /**
     * @return DisplayInterface
     */
    public function display()
    {
        return Display::dataTable([
                Column::text('id','ID'),
                Column::text('title','Код')
                    ->setSearchableCallback(function ($query,$search){
                        $query->where('title','like','%'.$search.'%');
                    }),
                Column::text('error','Ошибка'),
                Column::text('description_ru','Описание'),
            ])
            ->orderDefault(function ($query){
                $query->orderBy('id','asc');
            })
            ->setSearchable(true);
    }

    public function syncSettings()
    {
        try {
            (new SettingsSyncEntity())->syncErrorCodes();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
