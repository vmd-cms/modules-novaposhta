<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Models\Settings\PaymentForm;

class NovaPoshtaPaymentForm extends AbstractNovaPoshtaSettings
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_payment_form';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Формы оплаты";
    }

    public function getCmsModelClass(): string
    {
        return PaymentForm::class;
    }

    public function syncSettings()
    {
        try {
            (new SettingsSyncEntity())->syncPaymentForm();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
