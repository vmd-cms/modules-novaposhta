<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Models\Settings\ServiceType;

class NovaPoshtaServiceType extends AbstractNovaPoshtaSettings
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_service_type';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Технологии доставки";
    }

    public function getCmsModelClass(): string
    {
        return ServiceType::class;
    }

    public function syncSettings()
    {
        try {
            (new SettingsSyncEntity())->syncServiceTypes();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
