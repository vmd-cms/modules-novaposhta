<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Models\Settings\TypeOfPayer;

class NovaPoshtaTypeOfPayer extends AbstractNovaPoshtaSettings
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_type_of_payer';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Виды плательщиков";
    }

    public function getCmsModelClass(): string
    {
        return TypeOfPayer::class;
    }

    public function syncSettings()
    {
        try {
            (new SettingsSyncEntity())->syncTypeOfPayer();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
