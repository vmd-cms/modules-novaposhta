<?php

namespace VmdCms\Modules\NovaPoshta\Sections\Catalogs;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCustomButton;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\Entity\NovaPoshtaSync;
use VmdCms\Modules\NovaPoshta\Models\City;
use VmdCms\Modules\NovaPoshta\Models\Warehouse;

class NovaPoshtaWarehouse extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_warehouse';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Точки выдачи";
    }

    public function getCmsModelClass(): string
    {
        return Warehouse::class;
    }
    /**
     * @return DisplayInterface
     */
    public function display()
    {
        return Display::dataTable([
            Column::text('id','ID'),
            Column::text('ref','Идинтификатор')
                ->setSearchableCallback(function ($query,$search){
                    $query->where('ref','like','%'.$search.'%');
                }),
            Column::text('info.description','Описание')
            ->setSearchableCallback(function ($query,$search){
                $query->orWhereHas('info',function ($q) use ($search){
                    $q->where('description','like','%'.$search.'%');
                });
            }),
            Column::text('info.short_address','Адрес')
                ->setSearchableCallback(function ($query,$search){
                    $query->orWhereHas('info',function ($q) use ($search){
                        $q->where('short_address','like','%'.$search.'%');
                    });
                }),
            Column::text('info.city_description','Город')
                ->setSearchableCallback(function ($query,$search){
                    $query->orWhereHas('info',function ($q) use ($search){
                        $q->where('city_description','like','%'.$search.'%');
                    });
                }),
        ])->setSearchable(true);
    }

    public function getDisplayPanel()
    {
        return $this->display()
            ->setItemsPerPage(10)
            ->setShowActionColumn(false)
            ->setSection($this)
            ->setButtons([
                (new FormCustomButton('Синхронизировать'))
                    ->setRoutePath(route(CoreRouter::ROUTE_SERVICE_DATA,[
                        'sectionSlug' => $this->getSectionSlug(),
                        'sectionMethod' => 'syncSettings',
                        'id' => null
                    ]))
                    ->setGroupKey(FormButton::GROUP_ACTION_HEADER)
                    ->setAction(FormButton::ACTION_SERVICE_PATH)
                    ->setIconClass('icon-descargar')])
            ->getSectionPanel();
    }

    public function isCreatable(): bool
    {
        return false;
    }

    public function isViewable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return false;
    }
    public function syncSettings()
    {
        try {
            (new NovaPoshtaSync())->syncWarehouses();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
