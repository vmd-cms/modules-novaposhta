<?php

namespace VmdCms\Modules\NovaPoshta\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCustomButton;
use VmdCms\CoreCms\Dashboard\Forms\Components\ColumnComponent;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\DTO\Admin\DataCityDTO;
use VmdCms\Modules\NovaPoshta\DTO\Admin\DataWarehouseDTO;
use VmdCms\Modules\NovaPoshta\DTO\InternetDocumentDTO;
use VmdCms\Modules\NovaPoshta\Entity\InternetDocumentEntity;
use VmdCms\Modules\NovaPoshta\Entity\InternetDocumentHistoryEntity;
use VmdCms\Modules\NovaPoshta\Entity\NovaPoshtaInternetDocument;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Exceptions\AlreadyDeletedException;
use VmdCms\Modules\NovaPoshta\Exceptions\ApiException;
use VmdCms\Modules\NovaPoshta\Exceptions\SyncException;
use VmdCms\Modules\NovaPoshta\Exceptions\ValidationException;
use VmdCms\Modules\NovaPoshta\Models\City;
use VmdCms\Modules\NovaPoshta\Models\Settings\CargoType;
use VmdCms\Modules\NovaPoshta\Models\Settings\PaymentForm;
use VmdCms\Modules\NovaPoshta\Models\Settings\TypeOfPayer;
use VmdCms\Modules\NovaPoshta\Models\Warehouse;
use VmdCms\Modules\Orders\Models\Order;

class InternetDocument extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'nova_poshta_internet_documents';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Экспресс-накладные";
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('title','Title'),
            Column::text('active', 'Active'),
            Column::date('created_at', 'Created at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    public function isCreatable(): bool
    {
        return false;
    }

    /**
     * @param int|null $id
     * @return FormInterface
     * @throws CoreException
     * @throws ModelNotFoundException
     * @throws SyncException
     */
    public function edit(?int $id) : FormInterface
    {
        $internetDocument = \VmdCms\Modules\NovaPoshta\Models\InternetDocument::find($id);

        if(!$internetDocument instanceof \VmdCms\Modules\NovaPoshta\Models\InternetDocument){
            throw new ModelNotFoundException();
        }

        $order = Order::find($internetDocument->order_id);
        if(!$order instanceof Order){
            throw new ModelNotFoundException('Order not found');
        }

        $recipientData = !empty($order->user_data) && is_string($order->user_data) ? json_decode($order->user_data) : null;
        $recipientName = $recipientData && !empty($recipientData->firstName) ? $recipientData->firstName : '';
        $recipientName .= $recipientData && !empty($recipientData->lastName) ? ' ' . $recipientData->lastName : '';
        $recipientPhone = $recipientData && !empty($recipientData->phone) ? str_replace(' ','',$recipientData->phone) : null;

        $documentData = FormComponent::row('');

        $columnButtonsComponents = [
            FormComponent::buttons('')->appendButtons(
                (new FormCustomButton('Cохранить'))
                    ->setAction(FormButton::ACTION_SAVE)
                    ->setIconClass('icon-edit')),
        ];

        try {
           (new InternetDocumentEntity())->validateInternetDocumentFields($internetDocument);
            $columnButtonsComponents[] = FormComponent::buttons('')->appendButtons(
                (new FormCustomButton( empty($internetDocument->ref) ? 'Сгенирировать ЭН' : 'Обновить ЭН'))
                    ->setRoutePath(route(CoreRouter::ROUTE_SERVICE_DATA,[
                        'sectionSlug' => 'nova_poshta_internet_documents',
                        'sectionMethod' => 'createUpdateInternetDocument',
                        'id' => $internetDocument->id
                    ]))
                    ->setConfirmMessage('Сохраните изменения перед ' . (empty($internetDocument->ref) ? 'созданием' : 'обновлением') . ' Экспресс Накладной (ЭН). </br>Продолжить?')
                    ->setAction(FormButton::ACTION_SERVICE_PATH)
                    ->setIconClass('icon-calendar'));
        }catch (\Exception $exception){
            $documentData->appendColumn((new ColumnComponent(12))->appendComponents([
                FormComponent::alert('Заполните все обязательные поля и сохраните для того чтобы иметь возможность сгенерировать ЭН')->addClass('pt-5')->addClass('width-100pr max-width-100pr','component'),
            ]));
        }

        if($internetDocument->int_doc_number)
        {
            $link = route(CoreRouter::ROUTE_SERVICE_DATA_GET,[
                'sectionSlug' => 'nova_poshta_internet_documents',
                'sectionMethod' => 'printInternetDocument',
                'id' => $id],true);
            $columnButtonsComponents[] =  FormComponent::html('<div class="custom-block"><span class="fs-20px icon-print"></span><a target="_blank" class="pl-2 custom-link" href="' . $link . '" >Распечатать</a></div>');
        }

        if($this->isDeletable($internetDocument)){
            $columnButtonsComponents[] =  FormComponent::buttons('')->appendButtons(
                (new FormCustomButton('Удалить'))
                    ->setAction(FormButton::ACTION_DELETE)
                    ->setRoutePath(CoreRouter::getDeleteRoute($this->getSectionSlug(),$id))
                    ->setConfirmMessage('Удаление Экспресс Накладной (ЭН). </br>Продолжить?')
                    ->setIconClass('icon-delete'));
        }

        $counterparty = (new SettingsSyncEntity())->getCounterpartySenderContact();


        $documentData->appendColumn((new ColumnComponent(6))->appendComponents([
                FormComponent::html('<span class="light">Данные накладной </span>')->addClass('py-5'),
                FormComponent::input('ref','Ref номер накладной')->setDisabled(true),
                FormComponent::input('cost_on_site','Стоимость (грн)')->setDisabled(true),
                FormComponent::input('estimated_delivery_date','Ожидаемая дата доставки')->setDisabled(true),
                FormComponent::input('int_doc_number','Id накладной')->setDisabled(true),
                FormComponent::input('type_document','Тип документа')->setDisabled(true),
            ]))
            ->appendColumn((new ColumnComponent(3))->appendComponents($columnButtonsComponents)->addClass('pt-10'));

        $recipientSenderData = FormComponent::row('')
            ->appendColumn((new ColumnComponent(6))->appendComponents([
                FormComponent::html('<span class="light">Данные отправителя </span>')->addClass('py-5'),
                FormComponent::input('info_sender_full_name','ФИО отправителя')->setDefault($counterparty->full_name)->setDisabled(true),
                FormComponent::input('senders_phone','Телефон отправителя')->setDefault($counterparty->phones)->setDisabled(true),
                FormComponent::input('info_sender_city_title','Город отправителя')->setDefault($counterparty->city_title)->setDisabled(true),
                FormComponent::input('info_sender_warehouse_title','Отделение отправителя')->setDefault($counterparty->warehouse_title)->setDisabled(true),
            ]))
            ->appendColumn((new ColumnComponent(6))->appendComponents([
                FormComponent::html('<span class="light">Данные получателя </span>')->addClass('py-5'),
                FormComponent::input('recipient_name','ФИО получателя')->setDefault($recipientName)->required(),
                FormComponent::phone('recipients_phone','Телефон получателя')
                    ->setMask('##(###)###-##-##')
                    ->setDefault($recipientPhone)->setStoreCallback(function ($model){
                        $model->recipients_phone = preg_replace('~\D~', '', request()->get('recipients_phone'));
                    })->required(),
                $this->getRecipientAddressComponent($internetDocument),
            ]));

        $characters = FormComponent::row('')
            ->appendColumn((new ColumnComponent(6))->appendComponents([
                FormComponent::html('<span class="light">Характеристики отправки </span>')->addClass('py-5'),
                FormComponent::select('payer_type','Тип плательщика')->setDefault("Recipient")->setEnumValues(TypeOfPayer::getEnums())->required()->displayRow(),
                FormComponent::select('payment_method','Тип оплаты')->setDefault("Cash")->setEnumValues(PaymentForm::getEnums())->required()->displayRow(),
                FormComponent::select('cargo_type','Тип груза')->setDefault("Cargo")->setEnumValues(CargoType::getEnums())->required()->displayRow(),
                FormComponent::date('date_time','Дата отправки')->required()->displayRow(),
                FormComponent::input('description','Описание отправки')->maxLength(255)->required()->addClass('pt-5')->displayRow(),
            ]))
            ->appendColumn((new ColumnComponent(6))->appendComponents([
                FormComponent::html('<span class="light">Характеристики товара </span>')->addClass('py-5'),
                FormComponent::input('volume_general','Объем общий, м.куб')->float()->required()->displayRow(),
                FormComponent::input('weight','Общий вес посылки')->float()->required()->displayRow(),
                FormComponent::input('seats_amount','Общее количество мест')->integer()->required()->min(1)->displayRow(),
                FormComponent::input('cost','Сумма наложенного платежа')->setDefault($order->order_total)->float()->required()->displayRow(),
            ]));


        return Form::panel([
            FormComponent::html('')->setStoreCallback(function ($model) use ($counterparty){
                $model->service_type = 'WarehouseWarehouse';
                $model->sender = $counterparty->counterparty_ref;
                $model->senders_phone = preg_replace('~\D~', '', $counterparty->phones);

                $model->city_sender = $counterparty->city_ref;
                $model->contact_sender = $counterparty->contact_ref;
                $model->sender_address = $counterparty->warehouse_ref;

                $model->info_sender_full_name = $counterparty->full_name;
                $model->info_sender_city_title = $counterparty->city_title;
                $model->info_sender_warehouse_title = $counterparty->warehouse_title;
            }),
            $documentData,
            $recipientSenderData,
            $characters
        ])->hideFloatActionGroup()->hideFooterActionGroup();
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\Modules\NovaPoshta\Models\InternetDocument::class;
    }

    protected function getRecipientAddressComponent(\VmdCms\Modules\NovaPoshta\Models\InternetDocument $internetDocument)
    {
        $order = Order::find($internetDocument->order_id);
        if(!$order instanceof Order){
            throw new ModelNotFoundException('Order not found');
        }
        $deliveryData = !empty($order->delivery_data) && is_string($order->delivery_data) ? json_decode($order->delivery_data) : null;

        if(empty($internetDocument->recipient_city_name)){
            $internetDocument->recipient_city_name = $deliveryData && !empty($deliveryData->delivery_city_title) ? $deliveryData->delivery_city_title : null;
        }

        if(empty($internetDocument->recipient_city_id)){
            $internetDocument->recipient_city_id = $deliveryData && !empty($deliveryData->delivery_city_id) ? $deliveryData->delivery_city_id : null;
        }

        if(empty($internetDocument->recipient_city_ref)){
            $internetDocument->recipient_city_ref = $deliveryData && !empty($deliveryData->delivery_city_ref) ? $deliveryData->delivery_city_ref : null;
        }

        $dataCityDTO = new DataCityDTO(
            $internetDocument->recipient_city_name,
            $internetDocument->recipient_city_id,
            $internetDocument->recipient_city_ref
        );

        if(empty($internetDocument->info_recipient_warehouse_name)){
            $internetDocument->info_recipient_warehouse_name = $deliveryData && !empty($deliveryData->delivery_warehouse_title) ? $deliveryData->delivery_warehouse_title : null;
        }

        if(empty($internetDocument->info_recipient_warehouse_ref)){
            $internetDocument->info_recipient_warehouse_ref = $deliveryData && !empty($deliveryData->delivery_warehouse_ref) ? $deliveryData->delivery_warehouse_ref : null;
        }

        $dataWarehouseDTO = new DataWarehouseDTO(
            $internetDocument->info_recipient_warehouse_name,
            $internetDocument->info_recipient_warehouse_ref,
        );

        return FormComponent::view('')
            ->setViewPath('vmd_cms::admin.services.novaposhta.wrapper')
            ->setData([
                'dataCityDTO' => $dataCityDTO,
                'dataWarehouseDTO' => $dataWarehouseDTO
            ])
            ->setStoreCallback(function ($model) {
                $city = City::where('ref',request()->get('delivery_city_id'))->with('info')->first();
                $model->recipient_city_name = $city->info->description ?? request()->get('delivery_city_title');
                $model->recipient_city_id = request()->get('delivery_city_id');
                $model->recipient_city_ref = request()->get('delivery_city_ref');

                $warehouse = Warehouse::where('ref',request()->get('delivery_warehouse_ref'))->first();
                $model->recipient_address_name = $warehouse->number ?? null;
                $model->info_recipient_warehouse_name = request()->get('delivery_warehouse_title');
                $model->info_recipient_warehouse_ref = request()->get('delivery_warehouse_ref');
            });
    }

    public function createUpdateInternetDocument()
    {
        $internetDocument = \VmdCms\Modules\NovaPoshta\Models\InternetDocument::find(request()->id);
        if(!$internetDocument instanceof \VmdCms\Modules\NovaPoshta\Models\InternetDocument){
            return ApiResponse::error('Накладная не найденна');
        }

        try{

            (new InternetDocumentEntity())->validateInternetDocumentFields($internetDocument);

            $isNew = $internetDocument->ref ? true : false;
            $dto = new InternetDocumentDTO($internetDocument);
            $apiResponse = (new NovaPoshtaInternetDocument())->createOrUpdateInternetDocument($dto);

            if(empty($apiResponse)){
                throw new ApiException('Ошибка ' . ($isNew ? 'Создания' : 'Обновления') . ' ЭН',409);
            }

            $internetDocument->ref = $apiResponse->Ref ?? null;
            $internetDocument->cost_on_site = $apiResponse->CostOnSite ?? null;
            $internetDocument->estimated_delivery_date = $apiResponse->EstimatedDeliveryDate ?? null;
            $internetDocument->int_doc_number = $apiResponse->IntDocNumber ?? null;
            $internetDocument->type_document = $apiResponse->TypeDocument ?? null;

            $internetDocument->save();

            (new InternetDocumentHistoryEntity())->storeHistory($internetDocument);

        }catch (ValidationException $exception){
            return ApiResponse::error($exception->getMessage(),409);
        }catch (ApiException $exception){
            return ApiResponse::error($exception->getMessage(),$exception->getCode());
        }catch (\Exception $exception){
            return ApiResponse::error('Ошибка создания накладной');
        }

        return ApiResponse::success([
            'location_reload' => true,
            'message' => 'Успешно ' . ($isNew ? 'создано' : 'обновлено')
        ]);
    }

    public function printInternetDocument(){

        $internetDocumentId = \request()->id ?? null;

        if(empty($internetDocumentId)){
            abort(404);
        }

        $internetDocument = \VmdCms\Modules\NovaPoshta\Models\InternetDocument::where('id',$internetDocumentId)->first();

        if(!$internetDocument instanceof \VmdCms\Modules\NovaPoshta\Models\InternetDocument){
            abort(404);
        }

        $content = (new NovaPoshtaInternetDocument())->getPrintInternetDocument($internetDocument);

        if(empty($content)){
            abort(404);
        }

        return $content;
    }

    public function onDeleted(CmsModelInterface $model): void
    {
        $dto = $model instanceof \VmdCms\Modules\NovaPoshta\Models\InternetDocument ? new InternetDocumentDTO($model) : null;
        if($dto instanceof InternetDocumentDTO && !empty($dto->getRef())){
            try{
                (new NovaPoshtaInternetDocument())->deleteInternetDocument($dto);
            }
            catch (AlreadyDeletedException $exception){}
        }
        parent::onDeleted($model);
    }

    /**
     * @param CmsModelInterface $model
     * @return mixed
     */
    public function deleteResponse(CmsModelInterface $model){
        return ApiResponse::success([
            'message' => 'Successfully deleted',
            'location_redirect' => CoreRouter::getEditRoute('orders',$model->order_id)
        ]);
    }

}
