<?php

namespace VmdCms\Modules\NovaPoshta\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\MockCmsModel;
use VmdCms\CoreCms\Dashboard\Display\Panels\SectionPanel;
use VmdCms\CoreCms\Dashboard\Display\PanelsDisplayWrapper;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;
use VmdCms\Modules\NovaPoshta\Panels\CargoTypePanel;
use VmdCms\Modules\NovaPoshta\Panels\CityPanel;
use VmdCms\Modules\NovaPoshta\Panels\ErrorCodePanel;
use VmdCms\Modules\NovaPoshta\Panels\PaymentFormPanel;
use VmdCms\Modules\NovaPoshta\Panels\ServiceTypePanel;
use VmdCms\Modules\NovaPoshta\Panels\TypeOfPayerPanel;
use VmdCms\Modules\NovaPoshta\Panels\WarehousePanel;

class NovaPoshtaCatalogs extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'nova_poshta_catalogs';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Справочники";
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $display = new PanelsDisplayWrapper();

        $display->setSectionPanel(
            (new SectionPanel())
                ->setAdditionalPanels([
                    (new TypeOfPayerPanel()),
                    (new PaymentFormPanel()),
                    (new CargoTypePanel()),
                    (new ServiceTypePanel()),
                    (new CityPanel()),
                    (new WarehousePanel()),
                    (new ErrorCodePanel())->setColsMd(12),
                ]),
        );

        return $display;
    }

    public function isCreatable(): bool
    {
        return false;
    }

    public function isViewable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return false;
    }

    public function getCmsModelClass(): string
    {
        return MockCmsModel::class;
    }

}
