<?php

namespace VmdCms\Modules\NovaPoshta\Sections;

use VmdCms\CoreCms\Collections\FormTabsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\MockCmsModel;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCancelButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveButton;
use VmdCms\CoreCms\Dashboard\Forms\Components\FormTab;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;
use VmdCms\Modules\NovaPoshta\DTO\Admin\DataCityDTO;
use VmdCms\Modules\NovaPoshta\DTO\Admin\DataWarehouseDTO;
use VmdCms\Modules\NovaPoshta\Entity\NovaPoshtaSync;
use VmdCms\Modules\NovaPoshta\Entity\SettingsSyncEntity;
use VmdCms\Modules\NovaPoshta\Models\Settings\CounterpartySenderContact;

class NovaPoshtaSettings extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'nova_poshta_settings';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Настройки";
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $tabs = new FormTabsCollection();

        $tabItems = [];

        if($tabContact = $this->getCounterpartyTab()){
            $tabItems[] = $tabContact;
        }

        $tabs->appendItems($tabItems);

        return Form::tabbed($tabs)
            ->setSectionSlug($this->slug)
            ->setAction(CoreRouter::getDisplayRoute($this->slug))
            ->setMethod('POST')
            ->setButtons([
                new FormSaveButton(),
                new FormCancelButton()
            ]);
    }

    public function isCreatable(): bool
    {
        return false;
    }

    public function isViewable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return false;
    }

    public function getCmsModelClass(): string
    {
        return MockCmsModel::class;
    }

    protected function getCounterpartyTab()
    {

        try {
            $counterparty = (new SettingsSyncEntity())->getCounterpartySenderContact();
        }catch (\Exception $exception){
            return null;
        }

        $addressComponent = $this->getAddressComponent($counterparty);

        return new FormTab('Отправитель',[
            FormComponent::input('full_name','ФИО отправителя')->setCmsModel($counterparty)->setDisabled(true),
            FormComponent::input('phones','Телефон отправителя')->setCmsModel($counterparty)->setDisabled(true),
            $addressComponent
        ]);
    }

    protected function getAddressComponent(CounterpartySenderContact $counterparty)
    {
        $dataCityDTO = new DataCityDTO(
            $counterparty->city_title,
            $counterparty->city_delivery_id,
            $counterparty->city_ref
        );

        $dataWarehouseDTO = new DataWarehouseDTO(
            $counterparty->warehouse_title,
            $counterparty->warehouse_ref
        );

        return FormComponent::view('')
            ->setViewPath('vmd_cms::admin.services.novaposhta.wrapper')
            ->setData([
                'dataCityDTO' => $dataCityDTO,
                'dataWarehouseDTO' => $dataWarehouseDTO
            ])
            ->setStoreCallback(function ($model) use ($counterparty) {

                $counterparty->city_title = request()->get('delivery_city_title');
                $counterparty->city_delivery_id = request()->get('delivery_city_id');
                $counterparty->city_ref = request()->get('delivery_city_ref');

                $counterparty->warehouse_title = request()->get('delivery_warehouse_title');
                $counterparty->warehouse_ref = request()->get('delivery_warehouse_ref');

                $counterparty->save();

            });
    }

    public function syncCounterparty(){
        try {
            (new NovaPoshtaSync())->s();
            return ApiResponse::success(['location_reload' => true]);
        }catch (\Exception $exception){
            return ApiResponse::error($exception->getMessage());
        }
    }
}
