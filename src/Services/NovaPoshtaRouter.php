<?php

namespace VmdCms\Modules\NovaPoshta\Services;

class NovaPoshtaRouter
{
    const NOVAPOSHTA_PREFIX = 'novaposhta.';
    const ROUTE_NOVAPOSHTA_SEARCH_CITY = self::NOVAPOSHTA_PREFIX . 'search-city';
    const ROUTE_NOVAPOSHTA_SEARCH_WAREHOUSE = self::NOVAPOSHTA_PREFIX . 'search-warehouse';
}
