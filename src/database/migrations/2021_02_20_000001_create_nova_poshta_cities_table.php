<?php

use VmdCms\Modules\NovaPoshta\Models\City as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaPoshtaCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref',64)->nullable();
            $table->integer('delivery_1')->default(0)->nullable();
            $table->integer('delivery_2')->default(0)->nullable();
            $table->integer('delivery_3')->default(0)->nullable();
            $table->integer('delivery_4')->default(0)->nullable();
            $table->integer('delivery_5')->default(0)->nullable();
            $table->integer('delivery_6')->default(0)->nullable();
            $table->integer('delivery_7')->default(0)->nullable();
            $table->string('area',64)->nullable();
            $table->string('settlement_type',64)->nullable();
            $table->boolean('is_branch')->default(false)->nullable();
            $table->string('prevent_entry_new_streets_user')->nullable();
            $table->string('conglomerates',2000)->nullable();
            $table->string('city_id')->nullable();
            $table->string('special_cash_check')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
