<?php

use VmdCms\Modules\NovaPoshta\Models\Warehouse as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaPoshtaWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('sity_key',64)->nullable();
            $table->string('phone',128)->nullable();
            $table->string('type_of_warehouse',128)->nullable();
            $table->string('ref',128)->nullable();
            $table->string('number',64)->nullable();
            $table->string('city_ref',64)->nullable();
            $table->string('settlement_ref',64)->nullable();
            $table->string('longitude',64)->nullable();
            $table->string('latitude',64)->nullable();
            $table->string('post_finance',64)->nullable();
            $table->string('bicycle_parking',64)->nullable();
            $table->string('payment_access',64)->nullable();
            $table->string('pos_terminal',64)->nullable();
            $table->string('international_shipping',64)->nullable();
            $table->string('self_service_workplaces_count',64)->nullable();
            $table->string('total_max_weight_allowed',64)->nullable();
            $table->string('place_max_weight_allowed',64)->nullable();
            $table->string('sending_limitations_on_dimensions',2000)->nullable();
            $table->string('receiving_limitations_on_dimensions',2000)->nullable();
            $table->string('reception',2000)->nullable();
            $table->string('delivery',2000)->nullable();
            $table->string('schedule',2000)->nullable();
            $table->string('district_code',64)->nullable();
            $table->string('warehouse_status',64)->nullable();
            $table->string('warehouse_status_date',64)->nullable();
            $table->string('category_of_warehouse',64)->nullable();
            $table->string('direct',64)->nullable();
            $table->string('region_city',64)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
