<?php

use VmdCms\Modules\NovaPoshta\Models\InternetDocument as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaPoshtaInternetDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('moderator_id')->unsigned();
            $table->string('ref',64)->nullable();
            $table->string('cost_on_site',64)->nullable();
            $table->string('estimated_delivery_date',64)->nullable();
            $table->string('int_doc_number',64)->nullable();
            $table->string('type_document',64)->nullable();
            $table->string('payer_type',64)->nullable();
            $table->string('payment_method',64)->nullable();
            $table->string('cargo_type',64)->nullable();
            $table->float('volume_general')->nullable();
            $table->float('weight')->nullable();
            $table->string('service_type',64)->nullable();
            $table->string('seats_amount',64)->nullable();
            $table->string('description',64)->nullable();
            $table->float('cost')->nullable();
            $table->string('city_sender',64)->nullable();
            $table->string('sender',64)->nullable();
            $table->string('sender_address',64)->nullable();
            $table->string('contact_sender',64)->nullable();
            $table->string('senders_phone',64)->nullable();
            $table->string('recipient_city_name',64)->nullable();
            $table->string('recipient_city_id',64)->nullable();
            $table->string('recipient_city_ref',64)->nullable();
            $table->string('recipient_area',64)->nullable();
            $table->string('recipient_area_regions',64)->nullable();
            $table->string('recipient_address_name',64)->nullable();
            $table->string('recipient_house',64)->nullable();
            $table->string('recipient_flat',64)->nullable();
            $table->string('recipient_name',64)->nullable();
            $table->string('recipient_type',64)->nullable();
            $table->string('recipients_phone',64)->nullable();
            $table->string('date_time',64)->nullable();
            $table->string('settlement_type',64)->nullable();
            $table->string('info_sender_full_name',128)->nullable();
            $table->string('info_sender_city_title',64)->nullable();
            $table->string('info_sender_warehouse_title',64)->nullable();
            $table->string('info_recipient_warehouse_ref',128)->nullable();
            $table->string('info_recipient_warehouse_name',128)->nullable();
            $table->longText('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
