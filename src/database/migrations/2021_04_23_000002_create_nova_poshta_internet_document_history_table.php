<?php

use VmdCms\Modules\NovaPoshta\Models\InternetDocumentHistory as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaPoshtaInternetDocumentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->integer('nova_poshta_internet_document_id')->unsigned();
            $table->string('nova_poshta_internet_document_ref',64);
            $table->integer('moderator_id')->unsigned();
            $table->string('cost_on_site',64)->nullable();
            $table->string('estimated_delivery_date',64)->nullable();
            $table->string('int_doc_number',64)->nullable();
            $table->string('type_document',64)->nullable();
            $table->longText('dto')->nullable();
            $table->longText('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
