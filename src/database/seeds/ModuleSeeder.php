<?php
namespace VmdCms\Modules\NovaPoshta\database\seeds;

use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaCity;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaErrorCode;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaWarehouse;
use VmdCms\Modules\NovaPoshta\Sections\NovaPoshtaCatalogs;
use VmdCms\Modules\NovaPoshta\Sections\NovaPoshtaSettings;
use VmdCms\Modules\NovaPoshta\Sections\InternetDocument;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaCargoType;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaPaymentForm;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaServiceType;
use VmdCms\Modules\NovaPoshta\Sections\Catalogs\NovaPoshtaTypeOfPayer;

class ModuleSeeder extends AbstractModuleSeeder
{
    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            NovaPoshtaCatalogs::class => "Справочники",
            NovaPoshtaSettings::class => "Настройки",
            NovaPoshtaCargoType::class => "Виды груза",
            NovaPoshtaPaymentForm::class => "Формы оплаты",
            NovaPoshtaServiceType::class => "Технологии доставки",
            NovaPoshtaTypeOfPayer::class => "Виды плательщиков",
            NovaPoshtaCity::class => "Города",
            NovaPoshtaWarehouse::class => "Точки выдачи",
            NovaPoshtaErrorCode::class => "Коды ошибок",
            InternetDocument::class => "Экспресс-накладные"
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "configs",
                "title" => "Конфигурации",
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "nova_poshta",
                        "title" => "Api Новая Почта",
                        "icon" => "icon icon-shop",
                        "children" => [
                                [
                                    "slug" => "nova_poshta_settings",
                                    "section_class" => NovaPoshtaSettings::class,
                                    "title" => "Настройки",
                                ],
                                [
                                    "slug" => "nova_poshta_catalogs",
                                    "section_class" => NovaPoshtaCatalogs::class,
                                    "title" => "Справочники",
                                ]
                            ]

                    ]
                ]
            ],
        ];
    }

}
