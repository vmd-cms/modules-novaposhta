<div data-delivery-city-select class="row form-component">
    <div class="py-0 col-md-2 col-12">
        <label>Город</label>
    </div>
    <div class="custom-select-component py-0 col-md-10 col-12" data-delivery-select>
        <input type="hidden" data-selected-title value="{{isset($dataCityDTO) ? $dataCityDTO->getTitle() : null}}" name="delivery_city_title"/>
        <input type="hidden" data-selected-delivery-id value="{{isset($dataCityDTO) ? $dataCityDTO->getDeliveryId() : null}}" name="delivery_city_id"/>
        <input type="hidden" data-selected-form-element data-selected-ref value="{{isset($dataCityDTO) ? $dataCityDTO->getRef() : null}}" name="delivery_city_ref"/>

        <div class="custom-select-component__trigger">

            <input class="input-search" placeholder="Введите название города"
                   data-custom-select-component-triger
                   data-city-search-input
                   value="{{isset($dataCityDTO) ? $dataCityDTO->getTitle() : null}}">
            <div class="arrow"></div>
        </div>
        <div class="custom-options" data-options>
        </div>
    </div>
</div>

