@if(isset($options) && count($options))
    @foreach($options as $item)
        <span
            class="custom-option"
            data-option
            data-value="{{$item['title'] ?? null}}"
            data-delivery-id="{{$item['delivery_id'] ?? null}}"
            data-ref="{{$item['ref'] ?? null}}"
            >{{$item['title'] ?? null}}</span>
    @endforeach
@else
    <span class="custom-option">Ничего не найденно</span>
@endif
