<div data-delivery-warehouse-select  class="row form-component mt-5">
    <div class="py-0 col-md-2 col-12">
        <label>Отделение</label>
    </div>
    <div class="custom-select-component py-0 col-md-10 col-12 warehouse-select" data-delivery-select>
        <input type="hidden" data-selected-title value="{{isset($dataWarehouseDTO) ? $dataWarehouseDTO->getTitle() : null}}" name="delivery_warehouse_title"/>
        <input type="hidden" data-selected-form-element data-selected-ref value="{{isset($dataWarehouseDTO) ? $dataWarehouseDTO->getRef() : null}}" name="delivery_warehouse_ref"/>

        <div class="custom-select-component__trigger">

            <input class="input-search" placeholder="Выберите точку выдачи"
                   data-custom-select-component-triger
                   data-warehouse-search-input
                   value="{{isset($dataWarehouseDTO) ? $dataWarehouseDTO->getTitle() : null}}">
            <div class="arrow"></div>
        </div>
        <div class="custom-options" data-options data-page="1" data-has-next="true">
        </div>
    </div>
</div>

