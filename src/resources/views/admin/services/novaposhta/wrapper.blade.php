<div class="custom-contain-of-option" data-delivery-container>

    <div class="delivery-container" data-delivery-city-container>

            @include('vmd_cms::admin.services.novaposhta.city_select')

    </div>

    <div class="delivery-container" data-delivery-warehouse-container>

            @include('vmd_cms::admin.services.novaposhta.warehouse_select')

    </div>
</div>
