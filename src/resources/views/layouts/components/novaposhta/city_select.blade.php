<div data-delivery-city-select>
    <div class="checkout custom-select" data-delivery-select>
        <input type="hidden" data-selected-title value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getCityTitle() : null}}" name="delivery_city_title"/>
        <input type="hidden" data-selected-delivery-id value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getCityDeliveryId() : null}}" name="delivery_city_id"/>
        <input type="hidden" data-selected-form-element data-selected-ref value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getCityRef() : null}}" name="delivery_city_ref"/>

        <div class="custom-select__trigger">

            <input class="input-search" placeholder="Введите название города" data-city-search-input value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getCityTitle() : null}}">
            <div class="arrow"></div>
        </div>
        <div class="custom-options" data-options>
        </div>
    </div>
</div>

