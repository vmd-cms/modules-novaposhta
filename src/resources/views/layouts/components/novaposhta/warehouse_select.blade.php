<div data-delivery-warehouse-select>
    <div class="checkout custom-select warehouse-select" data-delivery-select>
        <input type="hidden" data-selected-title value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getWarehouseTitle() : null}}" name="delivery_warehouse_title"/>
        <input type="hidden" data-selected-form-element data-selected-ref value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getWarehouseRef() : null}}" name="delivery_warehouse_ref"/>

        <div class="custom-select__trigger">

            <input class="input-search" placeholder="Выберите точку выдачи" data-warehouse-search-input value="{{isset($userDeliveryDTO) ? $userDeliveryDTO->getWarehouseTitle() : null}}">
            <div class="arrow"></div>
        </div>
        <div class="custom-options" data-options data-page="1" data-has-next="true">
        </div>
    </div>
</div>

