<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
return function (){
    Route::group([
        'namespace' => "App\\Modules\\NovaPoshta\\Controllers",
    ],function (Router $router){
        $router->post('services/novaposhta/blocks', [
            'as'   => 'novaposhta-blocks',
            'uses' => 'AdminNovaPoshtaController@novaposhtaBlocks',
        ]);
        $router->post('services/novaposhta/city', [
            'as'   => 'novaposhta-search-city',
            'uses' => 'AdminNovaPoshtaController@searchCity',
        ]);
        $router->post('/services/novaposhta/warehouse', [
            'as'   => 'novaposhta-search-warehouse',
            'uses' => 'AdminNovaPoshtaController@searchWarehouse',
        ]);
    });
};
