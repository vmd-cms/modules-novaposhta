<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use VmdCms\Modules\NovaPoshta\Services\NovaPoshtaRouter;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\NovaPoshta\\Controllers",
    ],function (Router $router){
        $router->post('delivery/novaposhta/city', [
            'as'   => NovaPoshtaRouter::ROUTE_NOVAPOSHTA_SEARCH_CITY,
            'uses' => 'NovaPoshtaController@searchCity',
        ]);
        $router->post('delivery/novaposhta/warehouse', [
            'as'   => NovaPoshtaRouter::ROUTE_NOVAPOSHTA_SEARCH_WAREHOUSE,
            'uses' => 'NovaPoshtaController@searchWarehouse',
        ]);
    });
};
